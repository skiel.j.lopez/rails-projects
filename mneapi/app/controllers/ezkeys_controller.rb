class EzkeysController < ApplicationController
  respond_to :json, :html
  
  def index
    user = EzKey.where(ez_user_id: 616433)
    respond_to do |f|
      f.json { render json: { users: user } }
    end
  end

  def show
    user = EzKey.where(ez_user_id: params[:id])
    respond_to do |f|
      f.json { render json: { users: user } }
    end
  end
end
