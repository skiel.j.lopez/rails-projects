class EzKey < ActiveRecord::Base
  establish_connection Rails.application.config.database_configuration['development'][Rails.env]

  self.table_name = 'dbo.Ez_Key'
  self.primary_key = :ez_user_id

  # set_table_name 'dbo.Ez_Key'
  # set_primary_key 'ez_user_id'
end
