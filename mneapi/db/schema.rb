# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "ATGRegistrationSubmissions", primary_key: "ATGRegistrationSubmissionID", force: :cascade do |t|
    t.varchar  "FullName",       limit: 200,  null: false
    t.varchar  "Address",        limit: 500,  null: false
    t.varchar  "Phone",          limit: 20,   null: false
    t.varchar  "Email",          limit: 100,  null: false
    t.datetime "DateOfBirth",                 null: false
    t.varchar  "HighSchool",     limit: 100,  null: false
    t.datetime "GraduationDate",              null: false
    t.varchar  "Explanation",    limit: 2000, null: false
    t.boolean  "TravelAbroad",                null: false
    t.boolean  "ValidPassport",               null: false
    t.datetime "CreatedAt",                   null: false
    t.datetime "UpdatedAt",                   null: false
    t.datetime "DeletedAt"
  end

  create_table "Address", force: :cascade do |t|
    t.varchar "address1",    limit: 30
    t.varchar "address2",    limit: 30
    t.varchar "address3",    limit: 30
    t.varchar "state",       limit: 50
    t.varchar "city",        limit: 30
    t.varchar "zip",         limit: 10
    t.varchar "type",        limit: 1
    t.integer "traveler_id", limit: 4
    t.integer "guest_id",    limit: 4
    t.varchar "country",     limit: 50
  end

  create_table "Auction", primary_key: "ID", force: :cascade do |t|
    t.varchar "name",  limit: 50
    t.varchar "item1", limit: 5
    t.varchar "name2", limit: 50
    t.varchar "item2", limit: 5
    t.varchar "name3", limit: 50
    t.varchar "item3", limit: 5
    t.varchar "name4", limit: 50
    t.varchar "item4", limit: 5
    t.varchar "name5", limit: 50
    t.varchar "item5", limit: 5
    t.varchar "name6", limit: 50
    t.varchar "item6", limit: 5
    t.varchar "name7", limit: 50
    t.varchar "item7", limit: 5
    t.varchar "name8", limit: 50
    t.varchar "item8", limit: 5
  end

  create_table "BBTAVisitors", primary_key: "GBTAVisitorID", force: :cascade do |t|
    t.varchar  "BRM",                 limit: 200
    t.varchar  "Name",                limit: 200
    t.varchar  "Email",               limit: 200
    t.varchar  "Title",               limit: 200
    t.varchar  "Phone",               limit: 200
    t.varchar  "Company",             limit: 200
    t.varchar  "Provider",            limit: 200
    t.varchar  "ContractExpire",      limit: 200
    t.varchar  "GlobalTravelProgram", limit: 200
    t.varchar  "NumberOfCoutnries",   limit: 200
    t.varchar  "DomesticAirVolume",   limit: 200
    t.varchar  "GlobalAirVolume",     limit: 200
    t.varchar  "NumberOfTravelers",   limit: 200
    t.varchar  "OBTUsed",             limit: 200
    t.varchar  "Notes",               limit: 500
    t.datetime "CreatedAt",                       null: false
    t.datetime "UpdatedAt",                       null: false
    t.datetime "DeletedAt"
  end

  create_table "Book1", force: :cascade do |t|
    t.varchar "test2", limit: 255
    t.varchar "test",  limit: 50,  null: false
  end

  create_table "CC_Info", primary_key: "ID", force: :cascade do |t|
    t.varchar      "name",      limit: 50
    t.varchar      "number",    limit: 18
    t.varchar      "exp_mth",   limit: 2
    t.varchar      "exp_year",  limit: 4
    t.varchar      "type",      limit: 15
    t.varchar      "security",  limit: 5
    t.integer      "c_id",      limit: 4
    t.ss_timestamp "timestamp",            null: false
  end

  create_table "CDATA", id: false, force: :cascade do |t|
    t.char       "cfid", limit: 64
    t.char       "app",  limit: 64
    t.text_basic "data", limit: 2147483647
  end

  create_table "CGLOBAL", id: false, force: :cascade do |t|
    t.char       "cfid",   limit: 64
    t.text_basic "data",   limit: 2147483647
    t.datetime   "lvisit"
  end

  create_table "Cat", primary_key: "cat_id", force: :cascade do |t|
    t.varchar      "name",         limit: 20,                 null: false
    t.varchar      "owner",        limit: 20,                 null: false
    t.varchar      "species",      limit: 20,                 null: false
    t.datetime     "birth"
    t.datetime     "death"
    t.integer      "gender_id",    limit: 4,                  null: false
    t.datetime     "CreatedDate",                             null: false
    t.datetime     "ModifiedDate",                            null: false
    t.ss_timestamp "Timestamp",                               null: false
    t.boolean      "Active",                  default: true,  null: false
    t.boolean      "Hidden",                  default: false, null: false
  end

  create_table "Cat_SAVE_201402110836", id: false, force: :cascade do |t|
    t.integer      "cat_id",       limit: 4,                  null: false
    t.varchar      "name",         limit: 20,                 null: false
    t.varchar      "owner",        limit: 20,                 null: false
    t.varchar      "species",      limit: 20,                 null: false
    t.datetime     "birth"
    t.datetime     "death"
    t.integer      "gender_id",    limit: 4,                  null: false
    t.datetime     "CreatedDate",                             null: false
    t.datetime     "ModifiedDate",                            null: false
    t.ss_timestamp "Timestamp",                               null: false
    t.boolean      "Active",                  default: true,  null: false
    t.boolean      "Hidden",                  default: false, null: false
  end

  create_table "Cat_SAVE_201403171559", id: false, force: :cascade do |t|
    t.integer      "cat_id",       limit: 4,                  null: false
    t.varchar      "name",         limit: 20,                 null: false
    t.varchar      "owner",        limit: 20,                 null: false
    t.varchar      "species",      limit: 20,                 null: false
    t.datetime     "birth"
    t.datetime     "death"
    t.integer      "gender_id",    limit: 4,                  null: false
    t.datetime     "CreatedDate",                             null: false
    t.datetime     "ModifiedDate",                            null: false
    t.ss_timestamp "Timestamp",                               null: false
    t.boolean      "Active",                  default: true,  null: false
    t.boolean      "Hidden",                  default: false, null: false
  end

  create_table "ChucksHelperData", primary_key: "chuckshelperdata_id", force: :cascade do |t|
    t.datetime      "birthdate",                                                          null: false
    t.datetime      "dateofdeath"
    t.varchar       "fname",          limit: 50,                                          null: false
    t.varchar       "mname",          limit: 50
    t.varchar       "lname",          limit: 50,                                          null: false
    t.char          "gender",         limit: 2
    t.money         "stipend",                   precision: 19, scale: 4, default: 0.0
    t.decimal       "gpa",                       precision: 18, scale: 2, default: 0.0
    t.ss_timestamp  "ez_timestamp",                                                       null: false
    t.smalldatetime "ez_createstamp",                                                     null: false
    t.smalldatetime "ez_updatestamp",                                                     null: false
    t.boolean       "ez_active",                                          default: true,  null: false
    t.boolean       "ez_hidden",                                          default: false, null: false
  end

  create_table "Conference", primary_key: "ez_user_id", force: :cascade do |t|
    t.varchar      "fname",        limit: 25
    t.varchar      "lname",        limit: 25
    t.varchar      "email",        limit: 50
    t.varchar      "phone",        limit: 12
    t.varchar      "address",      limit: 30
    t.varchar      "address_cont", limit: 30
    t.varchar      "city",         limit: 50
    t.varchar      "state",        limit: 2
    t.varchar      "zip",          limit: 12
    t.varchar      "location",     limit: 10
    t.ss_timestamp "timestamp"
  end

  create_table "DEMO", id: false, force: :cascade do |t|
    t.integer "id",    limit: 4
    t.varchar "name",  limit: 50
    t.varchar "lname", limit: 50
    t.integer "cost",  limit: 4
    t.char    "phone", limit: 10
  end

  create_table "EZMailAdminUsers", primary_key: "AdminUserID", force: :cascade do |t|
    t.integer  "ez_user_id", limit: 4, null: false
    t.boolean  "IsManager"
    t.datetime "CreatedAt",            null: false
    t.datetime "UpdatedAt",            null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailAgentTravelEvents", primary_key: "AgentTravelEventID", force: :cascade do |t|
    t.integer  "TravelEventID", limit: 4, null: false
    t.integer  "ez_user_id",    limit: 4, null: false
    t.datetime "CreatedAt",               null: false
    t.datetime "UpdatedAt",               null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailCars", primary_key: "CarID", force: :cascade do |t|
    t.integer  "TravelerID",      limit: 4,   null: false
    t.varchar  "PickupCity",      limit: 100, null: false
    t.varchar  "PickupState",     limit: 100, null: false
    t.datetime "PickupDateTime",              null: false
    t.varchar  "DropoffCity",     limit: 100, null: false
    t.varchar  "DropoffState",    limit: 100, null: false
    t.datetime "DropoffDateTime",             null: false
    t.datetime "CreatedAt",                   null: false
    t.datetime "UpdatedAt",                   null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailFlights", primary_key: "FlightID", force: :cascade do |t|
    t.integer  "TravelerID",           limit: 4,   null: false
    t.integer  "TravelTypeID",         limit: 4,   null: false
    t.integer  "FlightSegmentNumber",  limit: 4,   null: false
    t.varchar  "FlightDepartureCity",  limit: 100, null: false
    t.varchar  "FlightDepartureState", limit: 100, null: false
    t.datetime "FlightDepartureDate",              null: false
    t.varchar  "FlightArrivalCity",    limit: 100, null: false
    t.varchar  "FlightArrivalState",   limit: 100, null: false
    t.varchar  "FlightPreferredTime",  limit: 10,  null: false
    t.varchar  "FlightTime",           limit: 10,  null: false
    t.datetime "CreatedAt",                        null: false
    t.datetime "UpdatedAt",                        null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailHotels", primary_key: "HotelID", force: :cascade do |t|
    t.integer  "TravelerID",        limit: 4,   null: false
    t.integer  "PreferredHotelID",  limit: 4
    t.varchar  "GDSPropertyNumber", limit: 10
    t.varchar  "HotelName",         limit: 100, null: false
    t.varchar  "HotelAddress",      limit: 100
    t.varchar  "HotelCity",         limit: 100, null: false
    t.varchar  "HotelState",        limit: 100
    t.varchar  "HotelZip",          limit: 20
    t.varchar  "HotelCountry",      limit: 50
    t.datetime "HotelCheckIn",                  null: false
    t.datetime "HotelCheckOut",                 null: false
    t.datetime "CreatedAt",                     null: false
    t.datetime "UpdatedAt",                     null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailPreferredHotels", primary_key: "PreferredHotelID", force: :cascade do |t|
    t.integer  "TravelEventID",         limit: 4
    t.integer  "ez_company_id",         limit: 4,   null: false
    t.varchar  "GDSPropertyNumber",     limit: 10
    t.varchar  "PreferredHotelName",    limit: 100, null: false
    t.varchar  "PreferredHotelAddress", limit: 100
    t.varchar  "PreferredHotelCity",    limit: 50,  null: false
    t.varchar  "PreferredHotelState",   limit: 50
    t.varchar  "PreferredHotelZip",     limit: 20
    t.varchar  "PreferredHotelCountry", limit: 50
    t.datetime "CreatedAt",                         null: false
    t.datetime "UpdatedAt",                         null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailRails", primary_key: "RailID", force: :cascade do |t|
    t.integer  "TravelerID",         limit: 4,   null: false
    t.integer  "TravelTypeID",       limit: 4,   null: false
    t.integer  "RailSegmentNumber",  limit: 4,   null: false
    t.varchar  "RailDepartureCity",  limit: 100, null: false
    t.varchar  "RailDepartureState", limit: 100, null: false
    t.datetime "RailDepartureDate",              null: false
    t.varchar  "RailArrivalCity",    limit: 100, null: false
    t.varchar  "RailArrivalState",   limit: 100, null: false
    t.varchar  "RailPreferredTime",  limit: 10,  null: false
    t.varchar  "RailTime",           limit: 10,  null: false
    t.datetime "CreatedAt",                      null: false
    t.datetime "UpdatedAt",                      null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailStatuses", primary_key: "StatusID", force: :cascade do |t|
    t.varchar  "Status",    limit: 50
    t.datetime "CreatedAt",            null: false
    t.datetime "UpdatedAt",            null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailTownCars", primary_key: "TownCarID", force: :cascade do |t|
    t.integer  "TravelerID",            limit: 4,   null: false
    t.integer  "TravelTypeID",          limit: 4,   null: false
    t.integer  "Passengers",            limit: 4,   null: false
    t.datetime "TownCarDate",                       null: false
    t.varchar  "TownCarPickupTime",     limit: 10,  null: false
    t.varchar  "TownCarDropoffTime",    limit: 10
    t.varchar  "TownCarPickupAddress",  limit: 200, null: false
    t.varchar  "TownCarDropoffAddress", limit: 200
    t.datetime "CreatedAt",                         null: false
    t.datetime "UpdatedAt",                         null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailTravelEvents", primary_key: "TravelEventID", force: :cascade do |t|
    t.integer  "ez_company_id",         limit: 4
    t.integer  "ez_country_id",         limit: 4
    t.varchar  "TravelEvent",           limit: 100, null: false
    t.integer  "IncludeTravelEventID",  limit: 4
    t.varchar  "FormTypesOfTravel",     limit: 50
    t.boolean  "IsTS24"
    t.integer  "ez_atg_agency_id",      limit: 4
    t.boolean  "FormTripPurposeOn"
    t.boolean  "FormTripPurposeListOn"
    t.boolean  "FormPrepopulateOn"
    t.varchar  "GDSStar",               limit: 50
    t.varchar  "ConfirmationEmailList", limit: 500
    t.varchar  "PrimaryLanguage",       limit: 5
    t.datetime "CreatedAt",                         null: false
    t.datetime "UpdatedAt",                         null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailTravelTypes", primary_key: "TravelTypeID", force: :cascade do |t|
    t.varchar  "TravelType", limit: 50, null: false
    t.datetime "CreatedAt",             null: false
    t.datetime "UpdatedAt",             null: false
    t.datetime "DeletedAt"
  end

  create_table "EZMailTravelers", primary_key: "TravelerID", force: :cascade do |t|
    t.integer  "StatusID",                  limit: 4,    null: false
    t.integer  "ez_user_id_Owner",          limit: 4
    t.varchar  "GroupID",                   limit: 50
    t.integer  "NumberInGroup",             limit: 4
    t.integer  "TravelEventID",             limit: 4,    null: false
    t.integer  "ez_user_id_Traveler",       limit: 4
    t.integer  "ez_user_id_SubmittedBy",    limit: 4
    t.integer  "TripPurposeID",             limit: 4
    t.varchar  "TripPurpose",               limit: 80
    t.varchar  "FirstName",                 limit: 50,   null: false
    t.varchar  "MiddleName",                limit: 50
    t.varchar  "LastName",                  limit: 50,   null: false
    t.varchar  "Email",                     limit: 50,   null: false
    t.varchar  "Phone",                     limit: 50,   null: false
    t.varchar  "Gender",                    limit: 1
    t.datetime "DateOfBirth"
    t.varchar  "SpecialRequests",           limit: 2000
    t.varchar  "AdditionalItineraryEmails", limit: 500
    t.datetime "CreatedAt",                              null: false
    t.datetime "UpdatedAt",                              null: false
    t.datetime "DeletedAt"
    t.varchar  "TravelType",                limit: 50
    t.varchar  "Department",                limit: 100
    t.varchar  "TravelIsFor",               limit: 50
    t.boolean  "UseDefaultCostCenter"
    t.varchar  "CostCenter",                limit: 100
    t.boolean  "UseDefaultCompanyCode"
    t.varchar  "CompanyCode",               limit: 100
    t.varchar  "ProjectNumber",             limit: 100
    t.varchar  "TaskCode",                  limit: 100
    t.varchar  "IONumber",                  limit: 150
    t.boolean  "ForGovernment"
    t.varchar  "GovernmentOfficialName",    limit: 100
    t.varchar  "GovernmentTravelReason",    limit: 100
    t.varchar  "EmployeeNumber",            limit: 50
    t.boolean  "UseDefaultAuthorizerEmail"
    t.varchar  "AuthorizerEmail",           limit: 150
  end

  create_table "EZMailTripPurposes", primary_key: "TripPurposeID", force: :cascade do |t|
    t.integer  "TravelEventID", limit: 4
    t.varchar  "TripPurpose",   limit: 100
    t.datetime "CreatedAt",                 null: false
    t.datetime "UpdatedAt",                 null: false
    t.datetime "DeletedAt"
  end

  create_table "Ez_1000", id: false, force: :cascade do |t|
    t.integer "ID", limit: 4, null: false
  end

  create_table "Ez_ATG_Agency", primary_key: "ez_atg_agency_id", force: :cascade do |t|
    t.varchar  "ez_atg_agency",           limit: 100
    t.integer  "ez_number_of_locations",  limit: 4
    t.varchar  "ez_agency_address_1",     limit: 500
    t.varchar  "ez_agency_address_2",     limit: 500
    t.varchar  "ez_agency_address_3",     limit: 500
    t.varchar  "ez_agency_address_4",     limit: 500
    t.varchar  "ez_agency_address_5",     limit: 500
    t.varchar  "ez_agency_address_6",     limit: 500
    t.varchar  "ez_agency_address_7",     limit: 500
    t.varchar  "ez_agency_address_8",     limit: 500
    t.integer  "ez_number_of_executives", limit: 4
    t.varchar  "ez_executive_name_1",     limit: 100
    t.varchar  "ez_executive_title_1",    limit: 100
    t.varchar  "ez_executive_email_1",    limit: 150
    t.varchar  "ez_executive_phone_1",    limit: 20
    t.varchar  "ez_executive_role_1",     limit: 500
    t.varchar  "ez_executive_name_2",     limit: 100
    t.varchar  "ez_executive_title_2",    limit: 100
    t.varchar  "ez_executive_email_2",    limit: 150
    t.varchar  "ez_executive_phone_2",    limit: 20
    t.varchar  "ez_executive_role_2",     limit: 500
    t.varchar  "ez_executive_name_3",     limit: 100
    t.varchar  "ez_executive_title_3",    limit: 100
    t.varchar  "ez_executive_email_3",    limit: 150
    t.varchar  "ez_executive_phone_3",    limit: 20
    t.varchar  "ez_executive_role_3",     limit: 500
    t.varchar  "ez_executive_name_4",     limit: 100
    t.varchar  "ez_executive_title_4",    limit: 100
    t.varchar  "ez_executive_email_4",    limit: 150
    t.varchar  "ez_executive_phone_4",    limit: 20
    t.varchar  "ez_executive_role_4",     limit: 500
    t.varchar  "ez_number_of_emps",       limit: 50
    t.varchar  "ez_number_of_agents",     limit: 50
    t.varchar  "ez_annual_revenue",       limit: 50
    t.varchar  "ez_number_of_clients",    limit: 50
    t.varchar  "ez_back_office_system",   limit: 200
    t.boolean  "ez_247"
    t.varchar  "ez_service_delievery",    limit: 500
    t.varchar  "ez_air_spend",            limit: 50
    t.varchar  "ez_website",              limit: 300
    t.varchar  "ez_gds_list",             limit: 500
    t.varchar  "ez_pcc_office_list",      limit: 500
    t.varchar  "ez_obt_list",             limit: 500
    t.varchar  "ez_profile_tools",        limit: 500
    t.varchar  "ez_languages",            limit: 500
    t.datetime "ez_createstamp"
    t.datetime "ez_updatestamp"
    t.boolean  "ez_active"
    t.boolean  "ez_hidden"
  end

  create_table "Ez_ATG_Agency_Event", primary_key: "ez_atg_agency_event_id", force: :cascade do |t|
    t.integer  "ez_atg_agency_id", limit: 4
    t.integer  "ez_event_id",      limit: 4
    t.datetime "ez_createstamp"
    t.datetime "ez_updatestamp"
    t.boolean  "ez_active"
    t.boolean  "ez_hidden"
  end

  create_table "Ez_ATG_Company_Country", primary_key: "ez_atg_client_country_id", force: :cascade do |t|
    t.integer  "ez_atg_agecny_id", limit: 4
    t.integer  "ez_company_id",    limit: 4
    t.integer  "ez_country_id",    limit: 4
    t.datetime "ez_createstamp"
    t.datetime "ez_updatestamp"
    t.boolean  "ez_active"
    t.boolean  "ez_hidden"
  end

  create_table "Ez_Accessories", primary_key: "ez_accessories_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.integer       "ez_event_id",                   limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.varchar       "accessories_description",       limit: 100,                 null: false
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Accessories_Detail_bkp", primary_key: "ez_accessories_detail_id", force: :cascade do |t|
    t.integer       "ez_accessories_id",          limit: 4,                  null: false
    t.integer       "ez_event_id",                limit: 4,                  null: false
    t.integer       "ez_user_id",                 limit: 4,                  null: false
    t.varchar       "ez_lk_air",                  limit: 30
    t.varchar       "ez_air_specail_req",         limit: 30
    t.varchar       "ez_air_remarks",             limit: 30
    t.varchar       "ez_lk_car",                  limit: 30
    t.varchar       "ez_car_special_req",         limit: 30
    t.varchar       "ez_car_remarks",             limit: 30
    t.varchar       "ez_lk_htl",                  limit: 30
    t.varchar       "ez_htl_special_req",         limit: 30
    t.varchar       "ez_htl_remarks",             limit: 30
    t.varchar       "ez_lk_rail",                 limit: 30
    t.varchar       "ez_rail_special_req",        limit: 30
    t.varchar       "ez_rail_remarks",            limit: 30
    t.varchar       "ez_lk_client_detail",        limit: 30
    t.varchar       "ez_lk_clothing",             limit: 30
    t.varchar       "ez_lk_badge",                limit: 30
    t.varchar       "ez_lk_immigration",          limit: 30
    t.varchar       "ez_lk_club",                 limit: 30
    t.varchar       "ez_lk_spa",                  limit: 30
    t.varchar       "ez_lk_meal",                 limit: 30
    t.varchar       "ez_lk_golf",                 limit: 30
    t.varchar       "ez_lk_attending",            limit: 30
    t.varchar       "ez_lk_runner",               limit: 30
    t.varchar       "ez_lk_tour",                 limit: 30
    t.varchar       "ez_lk_school",               limit: 30
    t.varchar       "ez_template_type",           limit: 30
    t.varchar       "ez_event_authentication_id", limit: 20,                 null: false
    t.smalldatetime "ez_createstamp",                                        null: false
    t.ss_timestamp  "ez_timestamp",                                          null: false
    t.boolean       "ez_active",                             default: false, null: false
  end

  create_table "Ez_Action", primary_key: "ez_action_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.boolean       "ez_event_attending",                        default: true,  null: false
    t.boolean       "ez_guest_attending"
    t.boolean       "ez_golf_attending"
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Action_Bridge", primary_key: "ez_action_bridge_id", force: :cascade do |t|
    t.integer       "ez_action_id",                  limit: 4,                   null: false
    t.integer       "ez_user_bridge_id",             limit: 4,                   null: false
    t.varchar       "ez_event_authentication_value", limit: 128,                 null: false
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Activity", primary_key: "ez_activity_id", force: :cascade do |t|
    t.integer       "ez_event_id",                   limit: 4,                                            null: false
    t.integer       "ez_company_id",                 limit: 4,                                            null: false
    t.varchar       "activity_name",                 limit: 100,                                          null: false
    t.varchar       "activity_code",                 limit: 5
    t.varchar       "activity_description",          limit: 300
    t.varchar       "activity_type",                 limit: 50
    t.varchar       "am_or_pm",                      limit: 3
    t.varchar       "time_period",                   limit: 50
    t.smalldatetime "activity_startdate"
    t.smalldatetime "activity_enddate"
    t.varchar       "activity_starttime",            limit: 8
    t.varchar       "activity_endtime",              limit: 8
    t.varchar       "min_number_tickets",            limit: 4
    t.varchar       "max_number_tickets",            limit: 4
    t.decimal       "activity_cost",                             precision: 18, scale: 4
    t.decimal       "activity_tax",                              precision: 18, scale: 4
    t.decimal       "activity_discount_cost",                    precision: 18, scale: 4
    t.varchar       "activity_remarks",              limit: 250
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                                                     null: false
    t.smalldatetime "ez_updatestamp",                                                                     null: false
    t.ss_timestamp  "ez_timestamp",                                                                       null: false
    t.boolean       "ez_active",                                                          default: true,  null: false
    t.boolean       "ez_hidden",                                                          default: false, null: false
  end

  create_table "Ez_Activity_Detail", primary_key: "ez_activity_detail_id", force: :cascade do |t|
    t.integer       "ez_activity_id",                limit: 4,                                            null: false
    t.integer       "ez_user_id",                    limit: 4,                                            null: false
    t.integer       "ez_event_id",                   limit: 4,                                            null: false
    t.integer       "ez_company_id",                 limit: 4,                                            null: false
    t.boolean       "attending",                                                          default: false
    t.boolean       "pending",                                                            default: false
    t.varchar       "ticket_for",                    limit: 100
    t.char          "number_tickets",                limit: 3
    t.decimal       "actual_cost_per",                           precision: 18, scale: 4
    t.varchar       "remarks",                       limit: 250
    t.integer       "ez_wait_list_id",               limit: 4
    t.varchar       "ez_event_authentication_value", limit: 128,                                          null: false
    t.smalldatetime "ez_createstamp",                                                                     null: false
    t.smalldatetime "ez_updatestamp",                                                                     null: false
    t.ss_timestamp  "ez_timestamp",                                                                       null: false
    t.boolean       "ez_active",                                                          default: true,  null: false
    t.boolean       "ez_hidden",                                                          default: false, null: false
  end

  create_table "Ez_Activity_Detail_v3", primary_key: "ez_activity_detail_id", force: :cascade do |t|
    t.integer       "ez_activity_id",                limit: 4,                                            null: false
    t.integer       "ez_user_id",                    limit: 4,                                            null: false
    t.integer       "ez_event_id",                   limit: 4,                                            null: false
    t.integer       "ez_company_id",                 limit: 4,                                            null: false
    t.boolean       "attending",                                                          default: false
    t.boolean       "pending",                                                            default: false
    t.varchar       "ticket_for",                    limit: 100
    t.char          "number_tickets",                limit: 3
    t.decimal       "actual_cost_per",                           precision: 18, scale: 4
    t.varchar       "remarks",                       limit: 250
    t.integer       "ez_wait_list_id",               limit: 4
    t.varchar       "ez_event_authentication_value", limit: 128,                                          null: false
    t.smalldatetime "ez_createstamp",                                                                     null: false
    t.smalldatetime "ez_updatestamp",                                                                     null: false
    t.ss_timestamp  "ez_timestamp",                                                                       null: false
    t.boolean       "ez_active",                                                          default: true,  null: false
    t.boolean       "ez_hidden",                                                          default: false, null: false
  end

  create_table "Ez_Address", primary_key: "ez_address_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.varchar       "address_attn_to",               limit: 50
    t.varchar       "address_name",                  limit: 150
    t.varchar       "address1",                      limit: 255,                 null: false
    t.varchar       "address2",                      limit: 150
    t.varchar       "address3",                      limit: 150
    t.varchar       "city",                          limit: 100,                 null: false
    t.varchar       "state",                         limit: 100,                 null: false
    t.varchar       "zip",                           limit: 15,                  null: false
    t.varchar       "mailcode",                      limit: 50
    t.varchar       "country",                       limit: 50
    t.varchar       "ez_address_type",               limit: 3,   default: "BUS", null: false
    t.varchar       "ez_event_authentication_value", limit: 128,                 null: false
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Air", primary_key: "ez_air_id", force: :cascade do |t|
    t.integer       "ez_user_id",                     limit: 4,                             null: false
    t.integer       "ez_event_id",                    limit: 4,                             null: false
    t.integer       "ez_company_id",                  limit: 4,                             null: false
    t.varchar       "request_type",                   limit: 50
    t.boolean       "profile_required",                                  default: false
    t.varchar       "air_required",                   limit: 10
    t.varchar       "ez_bk_agent_code",               limit: 3
    t.varchar       "ez_tk_agent_code",               limit: 3
    t.integer       "ez_dependency_id",               limit: 4
    t.varchar       "ez_dk",                          limit: 50
    t.integer       "air_segment_num",                limit: 4,                             null: false
    t.boolean       "air_segment_connection",                            default: false,    null: false
    t.char          "air_domestic",                   limit: 1,          default: "D",      null: false
    t.varchar       "air_trip",                       limit: 15,                            null: false
    t.varchar       "air_trip_name",                  limit: 150
    t.char          "air_trip_type",                  limit: 1,          default: "D"
    t.varchar       "air_class_of_service",           limit: 15
    t.varchar       "air_vendor_name",                limit: 50
    t.varchar       "air_vendor_name2",               limit: 50
    t.varchar       "air_vendor_name3",               limit: 50
    t.char          "air_airline_code",               limit: 3
    t.varchar       "air_flight_number",              limit: 10
    t.varchar       "air_ticket_number",              limit: 20
    t.varchar       "air_invoice_number",             limit: 20
    t.smalldatetime "air_issue_date"
    t.smalldatetime "air_expiration_date"
    t.varchar       "air_departure_airport_code",     limit: 150
    t.varchar       "air_alt_departure_airport_code", limit: 150
    t.varchar       "air_departure_city",             limit: 50,                            null: false
    t.varchar       "air_departure_state",            limit: 50,                            null: false
    t.smalldatetime "air_departure_date",                                                   null: false
    t.varchar       "air_departure_time",             limit: 50
    t.varchar       "air_departure_by",               limit: 50
    t.varchar       "air_arrival_airport_code",       limit: 150
    t.varchar       "air_alt_arrival_airport_code",   limit: 150
    t.varchar       "air_arrival_city",               limit: 50
    t.varchar       "air_arrival_state",              limit: 50
    t.smalldatetime "air_arrival_date"
    t.varchar       "air_arrival_time",               limit: 50
    t.varchar       "air_arrival_by",                 limit: 50
    t.text_basic    "air_special_request",            limit: 2147483647
    t.text_basic    "air_justification",              limit: 2147483647
    t.varchar       "air_seat_type",                  limit: 15
    t.varchar       "air_meal",                       limit: 50
    t.float         "air_fare_basis"
    t.varchar       "air_fare_justification_code",    limit: 15
    t.float         "air_fare_lost"
    t.float         "air_fare_savings"
    t.float         "air_face_value"
    t.float         "air_base_fare"
    t.float         "air_high_fare"
    t.float         "air_low_fare"
    t.float         "air_full_fare"
    t.float         "air_net_fare"
    t.float         "air_discount"
    t.float         "air_tax"
    t.float         "air_ttl_cost"
    t.varchar       "air_direct_bill",                limit: 50
    t.varchar       "air_billing_code",               limit: 100
    t.varchar       "air_account_code",               limit: 50
    t.char          "air_budgeted",                   limit: 3
    t.float         "air_budget_amt"
    t.varchar       "air_payment_status",             limit: 15
    t.varchar       "air_invoice_num",                limit: 20
    t.boolean       "car_shuttle",                                       default: false
    t.varchar       "company_location",               limit: 100
    t.varchar       "record_locator",                 limit: 40,                            null: false
    t.varchar       "gds_record_locator",             limit: 40
    t.varchar       "vendor_locator",                 limit: 15
    t.varchar       "purpose_of_trip",                limit: 100
    t.float         "service_fee"
    t.varchar       "custom_field1",                  limit: 100
    t.varchar       "custom_field2",                  limit: 100
    t.varchar       "custom_field3",                  limit: 100
    t.varchar       "custom_field4",                  limit: 100
    t.varchar       "custom_field5",                  limit: 100
    t.varchar       "custom_field6",                  limit: 100
    t.varchar       "custom_field7",                  limit: 100
    t.varchar       "custom_field8",                  limit: 100
    t.varchar       "custom_field9",                  limit: 100
    t.varchar       "custom_field10",                 limit: 100
    t.boolean       "ez_non_refundable",                                 default: false
    t.boolean       "ez_exchange",                                       default: false
    t.boolean       "ez_mco",                                            default: false,    null: false
    t.boolean       "ez_void",                                           default: false
    t.boolean       "ez_error",                                          default: false
    t.varchar       "ez_event_authentication_value",  limit: 128,                           null: false
    t.smalldatetime "ez_createstamp",                                                       null: false
    t.smalldatetime "ez_updatestamp",                                                       null: false
    t.ss_timestamp  "ez_timestamp",                                                         null: false
    t.boolean       "ez_active",                                         default: true
    t.boolean       "ez_hidden",                                         default: false
    t.boolean       "ez_save_trip",                                      default: false
    t.varchar       "ez_rec_type",                    limit: 15,         default: "ONLINE"
    t.varchar       "airrequired",                    limit: 10
  end

  create_table "Ez_Air_Category", primary_key: "ez_air_category_id", force: :cascade do |t|
    t.integer       "ez_webpage_category_id",        limit: 4,                   null: false
    t.varchar       "ez_table_field_name",           limit: 150
    t.varchar       "ez_table_field_value",          limit: 150
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Air_Preferred", primary_key: "ez_air_preferred_id", force: :cascade do |t|
    t.integer       "ez_user_id",                 limit: 4
    t.varchar       "ez_vendor_number",           limit: 50
    t.varchar       "ez_vendor_name",             limit: 50
    t.varchar       "ez_vendor_code",             limit: 50
    t.varchar       "ez_vendor_addr",             limit: 150
    t.varchar       "ez_vendor_city",             limit: 100
    t.varchar       "ez_vendor_state",            limit: 50
    t.varchar       "ez_vendor_zip",              limit: 5
    t.varchar       "ez_vendor_phone",            limit: 20
    t.varchar       "ez_preferred_route_by_code", limit: 100
    t.smalldatetime "ez_createstamp",                                         null: false
    t.smalldatetime "ez_updatestamp",                                         null: false
    t.ss_timestamp  "ez_timestamp",                                           null: false
    t.boolean       "ez_active",                              default: true,  null: false
    t.boolean       "ez_hidden",                              default: false, null: false
  end

  create_table "Ez_Alert_Message", primary_key: "ez_alert_message_id", force: :cascade do |t|
    t.integer       "ez_company_id",             limit: 4
    t.integer       "ez_alert_type_id",          limit: 4,                          null: false
    t.boolean       "ez_default_message",                           default: true,  null: false
    t.text_basic    "ez_custom_message_path",    limit: 2147483647
    t.integer       "assigned_by_user_id",       limit: 4,                          null: false
    t.varchar       "assigned_by_user_fullname", limit: 100,                        null: false
    t.smalldatetime "ez_createstamp",                                               null: false
    t.smalldatetime "ez_updatestamp",                                               null: false
    t.boolean       "ez_active",                                    default: true
    t.boolean       "ez_hidden",                                    default: false
  end

  create_table "Ez_Alert_Type", primary_key: "ez_alert_type_id", force: :cascade do |t|
    t.varchar       "ez_type_name",   limit: 100,                 null: false
    t.varchar       "ez_type_code",   limit: 5
    t.smalldatetime "ez_createstamp",                             null: false
    t.smalldatetime "ez_updatestamp",                             null: false
    t.boolean       "ez_active",                  default: true
    t.boolean       "ez_hidden",                  default: false
  end

  create_table "Ez_Approved_Travel", primary_key: "ez_approved_travel_id", force: :cascade do |t|
    t.integer       "ez_company_id",                   limit: 4,                   null: false
    t.integer       "ez_event_type_id",                limit: 4,                   null: false
    t.integer       "ez_status_id",                    limit: 4,                   null: false
    t.integer       "ez_user_id",                      limit: 4,                   null: false
    t.integer       "ez_air_id",                       limit: 4
    t.integer       "ez_hotel_id",                     limit: 4
    t.integer       "ez_car_id",                       limit: 4
    t.varchar       "travel_authorization_number",     limit: 15,                  null: false
    t.varchar       "group_travel",                    limit: 5
    t.varchar       "approver_name",                   limit: 200,                 null: false
    t.varchar       "approver_email",                  limit: 200
    t.varchar       "approver_phone",                  limit: 30
    t.varchar       "business_unit",                   limit: 50
    t.varchar       "billing_account",                 limit: 250
    t.integer       "request_for_approval_count",      limit: 2,   default: 0
    t.varchar       "record_locator",                  limit: 15,                  null: false
    t.varchar       "gds_record_locator",              limit: 15
    t.smalldatetime "approved_timestamp"
    t.integer       "view_by_user_id",                 limit: 4
    t.smalldatetime "viewed_after_approved_timestamp"
    t.smalldatetime "ez_createstamp",                                              null: false
    t.smalldatetime "ez_updatestamp",                                              null: false
    t.ss_timestamp  "ez_timestamp",                                                null: false
    t.boolean       "ez_active",                                   default: true,  null: false
    t.boolean       "ez_hidden",                                   default: false, null: false
  end

  create_table "Ez_Assign_Planner", primary_key: "ez_assign_planner_id", force: :cascade do |t|
    t.integer       "ez_traveler_profile_id",               limit: 4, null: false
    t.integer       "ez_assignplanner_traveler_profile_id", limit: 4, null: false
    t.integer       "ez_event_id",                          limit: 4, null: false
    t.integer       "ez_company_id",                        limit: 4, null: false
    t.smalldatetime "ez_createstamp",                                 null: false
    t.smalldatetime "ez_updatestamp",                                 null: false
    t.ss_timestamp  "ez_timestamp",                                   null: false
  end

  create_table "Ez_Attachment", primary_key: "ez_attachment_id", force: :cascade do |t|
    t.varchar       "attachment_orgin_name",    limit: 100,                 null: false
    t.integer       "attachment_by_user_id",    limit: 4,                   null: false
    t.integer       "attachment_by_product_id", limit: 4,                   null: false
    t.varchar       "attachment_name",          limit: 100,                 null: false
    t.varchar       "attachment_description",   limit: 250
    t.varchar       "attachment_path",          limit: 250,                 null: false
    t.smalldatetime "attachment_createstamp",                               null: false
    t.smalldatetime "attachment_updatestamp",                               null: false
    t.boolean       "attachment_hidden",                    default: false, null: false
  end

  create_table "Ez_Attn", primary_key: "ez_attn_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.integer       "ez_event_id",                   limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4
    t.varchar       "ez_attn_description",           limit: 12,  default: "YES", null: false
    t.varchar       "ez_attn_reasons",               limit: 200
    t.varchar       "ez_attn_description2",          limit: 12
    t.varchar       "ez_attn_reasons2",              limit: 200
    t.varchar       "reservation_type",              limit: 10
    t.integer       "ez_dependency_id",              limit: 4
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Bit", primary_key: "ez_bit_id", force: :cascade do |t|
    t.integer       "ez_event_id",        limit: 4,                  null: false
    t.boolean       "ez_bit_name"
    t.varchar       "ez_bit_description", limit: 50
    t.smalldatetime "ez_createstamp",                                null: false
    t.smalldatetime "ez_updatestamp",                                null: false
    t.ss_timestamp  "ez_timestamp",                                  null: false
    t.boolean       "ez_active",                     default: true
    t.boolean       "ez_hidden",                     default: false
  end

  create_table "Ez_Bit_Bridge", primary_key: "ez_bit_bridge_id", force: :cascade do |t|
    t.integer       "ez_bit_detail_id",  limit: 4,                 null: false
    t.integer       "ez_user_bridge_id", limit: 4
    t.smalldatetime "ez_createstamp",                              null: false
    t.smalldatetime "ez_updatestamp",                              null: false
    t.ss_timestamp  "ez_timestamp",                                null: false
    t.boolean       "ez_active",                   default: true
    t.boolean       "ez_hidden",                   default: false
  end

  create_table "Ez_Bit_Detail", primary_key: "ez_bit_detail_id", force: :cascade do |t|
    t.integer       "ez_bit_id",      limit: 4,                 null: false
    t.boolean       "ez_bit_data"
    t.integer       "ez_user_id",     limit: 4,                 null: false
    t.smalldatetime "ez_createstamp",                           null: false
    t.smalldatetime "ez_updatestamp",                           null: false
    t.ss_timestamp  "ez_timestamp",                             null: false
    t.boolean       "ez_active",                default: true
    t.boolean       "ez_hidden",                default: false
  end

  create_table "Ez_Blog", primary_key: "ez_blog_id", force: :cascade do |t|
    t.string        "blog_name",         limit: 150,                        null: false
    t.text_basic    "blog_description",  limit: 2147483647,                 null: false
    t.integer       "ez_company_id",     limit: 4,                          null: false
    t.varchar       "blogger",           limit: 200
    t.integer       "ez_project_id",     limit: 4
    t.integer       "ez_event_id",       limit: 4
    t.integer       "ez_product_id",     limit: 4
    t.smalldatetime "blog_deleted_date"
    t.smalldatetime "blog_createstamp",                                     null: false
    t.smalldatetime "blog_updatestamp",                                     null: false
    t.boolean       "blog_active",                          default: false, null: false
  end

  create_table "Ez_Blog_Category", primary_key: "ez_blog_category_id", force: :cascade do |t|
    t.varchar "blog_category",    limit: 50,                  null: false
    t.varchar "blog_description", limit: 500
    t.boolean "blog_active",                  default: false, null: false
    t.boolean "blog_hidden",                  default: false, null: false
  end

  create_table "Ez_Blog_Detail", primary_key: "ez_blog_detail_id", force: :cascade do |t|
    t.integer       "ez_blog_id",          limit: 4,                          null: false
    t.integer       "ez_user_id",          limit: 4,                          null: false
    t.integer       "ez_from_user_id",     limit: 4
    t.varchar       "blog_title",          limit: 250,                        null: false
    t.varchar_max   "blog_body",           limit: 2147483647,                 null: false
    t.boolean       "blog_posted",                            default: false
    t.boolean       "blog_resolution",                        default: false
    t.integer       "ez_status_id",        limit: 4
    t.varchar       "blog_avatar",         limit: 100
    t.integer       "ez_issue_id",         limit: 4
    t.integer       "ez_project_id",       limit: 4
    t.integer       "ez_file_mngt_id",     limit: 4
    t.integer       "ez_blog_category_id", limit: 4
    t.integer       "ez_close_by_user_id", limit: 4
    t.datetime      "starttime"
    t.datetime      "stoptime"
    t.smalldatetime "blog_createstamp",                                       null: false
    t.smalldatetime "blog_updatestamp",                                       null: false
    t.boolean       "blog_active",                            default: false, null: false
    t.boolean       "blog_hidden",                            default: false, null: false
  end

  create_table "Ez_CC", primary_key: "ez_cc_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.integer       "ez_event_id",                   limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.integer       "ez_travel_type_id",             limit: 4,                   null: false
    t.varchar       "name",                          limit: 200
    t.char          "type",                          limit: 20
    t.char          "type_code",                     limit: 2
    t.char          "number",                        limit: 32,                  null: false
    t.char          "expiration",                    limit: 32
    t.char          "code",                          limit: 32
    t.boolean       "cc_flag_read",                              default: false
    t.boolean       "cc_default_to",                             default: false
    t.boolean       "cc_default_web",                            default: false
    t.boolean       "cc_default_air",                            default: false
    t.boolean       "cc_default_htl",                            default: false
    t.boolean       "cc_default_car",                            default: false
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Car", primary_key: "ez_car_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                             null: false
    t.integer       "ez_event_id",                   limit: 4,                             null: false
    t.integer       "ez_company_id",                 limit: 4,                             null: false
    t.varchar       "request_type",                  limit: 50
    t.boolean       "profile_required",                                 default: false
    t.varchar       "car_required",                  limit: 10
    t.varchar       "ez_bk_agent_code",              limit: 3
    t.varchar       "ez_tk_agent_code",              limit: 3
    t.integer       "ez_dependency_id",              limit: 4
    t.varchar       "ez_dk",                         limit: 50
    t.integer       "car_segment_num",               limit: 4,                             null: false
    t.char          "car_domestic",                  limit: 1,          default: "D",      null: false
    t.varchar       "car_trip",                      limit: 15,                            null: false
    t.varchar       "car_trip_name",                 limit: 50
    t.char          "car_trip_type",                 limit: 1,          default: "D"
    t.char          "car_vendor_code",               limit: 3
    t.varchar       "car_vendor_name",               limit: 50
    t.varchar       "car_vendor_name2",              limit: 50
    t.varchar       "car_vendor_name3",              limit: 50
    t.varchar       "car_pick_up_city",              limit: 50,                            null: false
    t.varchar       "car_pick_up_state",             limit: 50,                            null: false
    t.smalldatetime "car_pick_up_date",                                                    null: false
    t.varchar       "car_pick_up_time",              limit: 10
    t.varchar       "car_pick_up_location",          limit: 250
    t.varchar       "car_drop_off_city",             limit: 50
    t.varchar       "car_drop_off_state",            limit: 50
    t.smalldatetime "car_drop_off_date",                                                   null: false
    t.varchar       "car_drop_off_time",             limit: 10
    t.varchar       "car_drop_off_location",         limit: 250
    t.smalldatetime "car_issue_date"
    t.text_basic    "car_special_request",           limit: 2147483647
    t.text_basic    "car_justification",             limit: 2147483647
    t.varchar       "car_size",                      limit: 50
    t.char          "car_smoking",                   limit: 1,          default: "Y",      null: false
    t.float         "car_rate"
    t.char          "car_days",                      limit: 10
    t.char          "car_number_of_cars",            limit: 10
    t.float         "car_ttl_cost"
    t.varchar       "car_direct_bill",               limit: 50
    t.varchar       "car_billing_code",              limit: 50
    t.varchar       "car_account_code",              limit: 50
    t.char          "car_budgeted",                  limit: 3
    t.float         "car_budget_amt"
    t.varchar       "car_invoice_num",               limit: 20
    t.varchar       "company_location",              limit: 100
    t.varchar       "record_locator",                limit: 40,                            null: false
    t.varchar       "gds_record_locator",            limit: 40
    t.varchar       "purpose_of_trip",               limit: 100
    t.float         "service_fee"
    t.varchar       "custom_field1",                 limit: 100
    t.varchar       "custom_field2",                 limit: 100
    t.varchar       "custom_field3",                 limit: 100
    t.varchar       "custom_field4",                 limit: 100
    t.varchar       "custom_field5",                 limit: 100
    t.varchar       "custom_field6",                 limit: 100
    t.varchar       "custom_field7",                 limit: 100
    t.varchar       "custom_field8",                 limit: 100
    t.varchar       "custom_field9",                 limit: 100
    t.varchar       "custom_field10",                limit: 100
    t.boolean       "ez_error",                                         default: false
    t.varchar       "ez_event_authentication_value", limit: 128,                           null: false
    t.smalldatetime "ez_createstamp",                                                      null: false
    t.smalldatetime "ez_updatestamp",                                                      null: false
    t.ss_timestamp  "ez_timestamp",                                                        null: false
    t.boolean       "ez_active",                                        default: true
    t.boolean       "ez_hidden",                                        default: false
    t.boolean       "ez_save_trip",                                     default: false
    t.varchar       "ez_rec_type",                   limit: 15,         default: "ONLINE"
  end

  create_table "Ez_Car_Category", primary_key: "ez_car_category_id", force: :cascade do |t|
    t.integer       "ez_webpage_category_id",        limit: 4,                   null: false
    t.varchar       "ez_table_field_name",           limit: 150
    t.varchar       "ez_table_field_value",          limit: 150
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Car_Preferred", primary_key: "ez_car_preferred_id", force: :cascade do |t|
    t.integer       "ez_user_id",                 limit: 4
    t.varchar       "ez_vendor_number",           limit: 50
    t.varchar       "ez_vendor_name",             limit: 50
    t.varchar       "ez_vendor_code",             limit: 50
    t.varchar       "ez_vendor_addr",             limit: 150
    t.varchar       "ez_vendor_city",             limit: 100
    t.varchar       "ez_vendor_state",            limit: 50
    t.varchar       "ez_vendor_zip",              limit: 5
    t.varchar       "ez_vendor_phone",            limit: 20
    t.varchar       "ez_preferred_route_by_code", limit: 100
    t.smalldatetime "ez_createstamp",                                         null: false
    t.smalldatetime "ez_updatestamp",                                         null: false
    t.ss_timestamp  "ez_timestamp",                                           null: false
    t.boolean       "ez_active",                              default: true,  null: false
    t.boolean       "ez_hidden",                              default: false, null: false
  end

  create_table "Ez_Class", primary_key: "ez_class_id", force: :cascade do |t|
    t.integer       "ez_company_id",                 limit: 4,                    null: false
    t.varchar       "ez_class_name",                 limit: 100,                  null: false
    t.varchar       "ez_class_hyperlink",            limit: 200
    t.varchar       "ez_class_description",          limit: 2000
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                             null: false
    t.smalldatetime "ez_updatestamp",                                             null: false
    t.ss_timestamp  "ez_timestamp",                                               null: false
    t.boolean       "ez_active",                                  default: true,  null: false
    t.boolean       "ez_hidden",                                  default: false, null: false
  end

  create_table "Ez_Class_Cost", primary_key: "ez_cost_id", force: :cascade do |t|
    t.integer       "ez_schedule_id",                limit: 4,                                           null: false
    t.integer       "ez_company_id",                 limit: 4,                                           null: false
    t.float         "ez_class_cost_per_seat"
    t.float         "ez_class_tax"
    t.decimal       "ez_class_discount",                         precision: 3, scale: 2
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                                                    null: false
    t.smalldatetime "ez_updatestamp",                                                                    null: false
    t.ss_timestamp  "ez_timestamp",                                                                      null: false
    t.boolean       "ez_active",                                                         default: true,  null: false
    t.boolean       "ez_hidden",                                                         default: false, null: false
  end

  create_table "Ez_Class_Instructor", primary_key: "ez_instructor_id", force: :cascade do |t|
    t.integer       "ez_schedule_id",                limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.varchar       "ez_instructor_code",            limit: 10
    t.varchar       "ez_instructor_fname",           limit: 100,                 null: false
    t.varchar       "ez_instructor_lname",           limit: 100,                 null: false
    t.varchar       "ez_instructor_email",           limit: 150,                 null: false
    t.nchar         "ez_instructor_second_email",    limit: 150
    t.varchar       "ez_instructor_description",     limit: 500
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Class_Material", primary_key: "ez_material_id", force: :cascade do |t|
    t.integer       "ez_schedule_id",                limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.varchar       "ez_material_name",              limit: 100,                 null: false
    t.char          "ez_material_code",              limit: 10
    t.varchar       "ez_material_description",       limit: 500
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Class_Schedule", primary_key: "ez_schedule_id", force: :cascade do |t|
    t.integer       "ez_class_id",                   limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.varchar       "ez_class_code",                 limit: 50
    t.smalldatetime "ez_class_start_date",                                       null: false
    t.smalldatetime "ez_class_end_date",                                         null: false
    t.varchar       "ez_class_max_seat",             limit: 50
    t.varchar       "ez_class_min_seat",             limit: 50,  default: "1",   null: false
    t.boolean       "ez_class_max_alert",                        default: false
    t.varchar       "ez_class_alert_email",          limit: 100
    t.boolean       "ez_class_wait_list",                        default: false
    t.boolean       "ez_class_reg_site",                         default: false
    t.varchar       "ez_class_remarks",              limit: 500
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Clothing", primary_key: "ez_clothing_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                                            null: false
    t.integer       "ez_event_id",                   limit: 4,                                            null: false
    t.integer       "ez_company_id",                 limit: 4,                                            null: false
    t.integer       "ez_accessories_id",             limit: 4
    t.boolean       "clothing_required",                                                  default: true,  null: false
    t.varchar       "ez_type",                       limit: 150,                                          null: false
    t.char          "ez_gender",                     limit: 1
    t.varchar       "ez_color",                      limit: 10
    t.varchar       "ez_size",                       limit: 50
    t.varchar       "ez_neck",                       limit: 50
    t.varchar       "ez_farbic",                     limit: 50
    t.varchar       "ez_sleeve",                     limit: 50,                           default: "0"
    t.varchar       "ez_lenght",                     limit: 50
    t.decimal       "ez_quanity",                                precision: 18, scale: 0
    t.boolean       "ez_gift_box"
    t.float         "ez_cloth_cost"
    t.decimal       "ez_cloth_discount",                         precision: 3,  scale: 0
    t.varchar       "ez_event_authentication_value", limit: 128,                                          null: false
    t.smalldatetime "ez_createstamp",                                                                     null: false
    t.smalldatetime "ez_updatestamp",                                                                     null: false
    t.ss_timestamp  "ez_timestamp",                                                                       null: false
    t.boolean       "ez_active",                                                          default: true,  null: false
    t.boolean       "ez_hidden",                                                          default: false, null: false
  end

  create_table "Ez_Clothing_Bridge", primary_key: "ez_clothing_bridge_id", force: :cascade do |t|
    t.integer "ez_user_bridge_id", limit: 4, null: false
    t.integer "ez_clothing_id",    limit: 4, null: false
  end

  create_table "Ez_Company", primary_key: "ez_company_id", force: :cascade do |t|
    t.integer       "ez_dependency_id",                 limit: 4
    t.varchar       "ez_company_name",                  limit: 50,                                                      null: false
    t.varchar       "ez_site_path",                     limit: 150,                                                     null: false
    t.varchar       "ez_gds_company_name",              limit: 50
    t.varchar       "ez_olb_company_name",              limit: 50
    t.varchar       "ez_olb_clone_login",               limit: 50
    t.varchar       "ez_company_email",                 limit: 150
    t.varchar       "ez_resx_pwd",                      limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",                    limit: 50
    t.varchar       "ez_alias_path",                    limit: 150
    t.varchar       "ez_logo_path",                     limit: 100,        default: "/logo-atg-small.png",              null: false
    t.varchar       "ez_homepage_img_path",             limit: 50,         default: "/login_form_bkgrnd_concourse.png"
    t.varchar       "ez_primary_color",                 limit: 10
    t.varchar       "ez_secondary_color",               limit: 10
    t.varchar       "ez_accent_color",                  limit: 10
    t.varchar       "ez_sso_key",                       limit: 200
    t.integer       "sso_key_expiration",               limit: 1
    t.varchar       "ez_hash_type",                     limit: 5
    t.text_basic    "ez_message_a",                     limit: 2147483647
    t.text_basic    "ez_message_b",                     limit: 2147483647
    t.varchar       "ez_ts_message",                    limit: 300
    t.boolean       "is_portal",                                           default: false
    t.varchar       "portal_ver",                       limit: 20
    t.boolean       "is_implemented",                                      default: false
    t.smalldatetime "implement_date"
    t.boolean       "is_in_use",                                           default: true,                               null: false
    t.varchar       "ez_travel_department_name",        limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone",       limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email",       limit: 100
    t.varchar       "ez_pkey",                          limit: 200
    t.varchar       "ip_address",                       limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",         limit: 150
    t.varchar       "define_login",                     limit: 50
    t.boolean       "is_global",                                           default: false
    t.boolean       "tracking_on",                                         default: false
    t.smalldatetime "ez_createstamp",                                                                                   null: false
    t.smalldatetime "ez_updatestamp",                                                                                   null: false
    t.varchar       "ez_update_by",                     limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                                     null: false
    t.boolean       "ez_active",                                           default: true
    t.boolean       "ez_hidden",                                           default: false
    t.varchar       "ez_homepage_default_country_abbr", limit: 100
    t.varchar       "ez_homepage_logo_path",            limit: 100
  end

  create_table "Ez_Company_Gds", primary_key: "ez_company_gds_id", force: :cascade do |t|
    t.integer       "ez_company_id",       limit: 4,                       null: false
    t.integer       "ez_olb_tool_id",      limit: 1,                       null: false
    t.integer       "ez_gds_id",           limit: 1,                       null: false
    t.varchar       "ez_feed_type",        limit: 15,  default: "STD"
    t.varchar       "ez_gds_company_name", limit: 50
    t.varchar       "ez_gds_site_path",    limit: 150
    t.varchar       "ez_olb_company_name", limit: 50
    t.varchar       "ez_olb_clone_login",  limit: 50
    t.varchar       "ez_olb_site_path",    limit: 150
    t.varchar       "ez_resx_pwd",         limit: 50,  default: "travel1"
    t.varchar       "ez_alias_path",       limit: 150
    t.smalldatetime "ez_createstamp",                                      null: false
    t.smalldatetime "ez_updatestamp",                                      null: false
    t.ss_timestamp  "ez_timestamp",                                        null: false
    t.boolean       "ez_active",                       default: true
    t.boolean       "ez_hidden",                       default: false
  end

  create_table "Ez_Company_Location", primary_key: "ez_company_loc_id", force: :cascade do |t|
    t.integer       "ez_company_id",                limit: 4,                          null: false
    t.varchar       "ez_loc_country_default",       limit: 50
    t.varchar       "ez_loc_language_default",      limit: 50
    t.varchar       "ez_loc_message_a",             limit: 8000
    t.string        "ez_loc_message_b",             limit: 4000
    t.text_basic    "ez_loc_ts_message",            limit: 2147483647
    t.boolean       "multi_language",                                  default: false
    t.boolean       "track_activity",                                  default: false
    t.boolean       "is_ezplan_customize",                             default: false
    t.smalldatetime "ez_loc_implement_date"
    t.varchar       "ez_loc_olb_tool",              limit: 15
    t.varchar_max   "ez_olb_url",                   limit: 2147483647
    t.varchar_max   "ez_olb_enc_key",               limit: 2147483647
    t.varchar       "ez_olb_site_name",             limit: 50
    t.integer       "ez_loc_olb_client_id",         limit: 4
    t.varchar       "ez_loc_olb_language_abbr",     limit: 5
    t.varchar       "ez_loc_olb_clone_login",       limit: 50
    t.varchar       "ez_loc_olb_pwd",               limit: 50
    t.varchar       "ez_loc_profile_tool",          limit: 15
    t.varchar_max   "ez_loc_profile_url",           limit: 2147483647
    t.varchar_max   "ez_loc_profile_enc_key",       limit: 2147483647
    t.varchar       "ez_loc_profile_site_name",     limit: 50
    t.varchar       "ez_loc_profile_site_id",       limit: 15
    t.varchar       "ez_loc_profile_hmcid",         limit: 15
    t.varchar       "ez_loc_profile_language_abbr", limit: 5
    t.varchar       "ez_loc_gds_name",              limit: 50
    t.varbinary     "ez_loc_gds_ipcc",              limit: 50
    t.varchar       "ez_loc_sso_key",               limit: 200
    t.varchar       "ez_loc_sso_code",              limit: 50
    t.varchar       "ez_loc_sso_supersite",         limit: 50
    t.varchar       "ez_loc_sso_subsite",           limit: 50
    t.integer       "ez_loc_sso_key_expiration",    limit: 1
    t.varchar       "ez_loc_hash_type",             limit: 5
    t.varchar       "ez_loc_abbr",                  limit: 5
    t.varchar       "ez_loc_default_flag",          limit: 50
    t.varchar       "ez_loc_second_language",       limit: 50
    t.varchar       "ez_loc_second_flag",           limit: 50
    t.varchar       "ez_loc_ip_address",            limit: 50
    t.varchar       "ez_loc_number",                limit: 50
    t.varchar       "ez_loc_after_number",          limit: 50
    t.varchar       "ez_loc_name",                  limit: 100
    t.varchar       "ez_loc_addr",                  limit: 150
    t.varchar       "ez_loc_addr2",                 limit: 50
    t.varchar       "ez_loc_city",                  limit: 100
    t.varchar       "ez_loc_state",                 limit: 100
    t.varchar       "ez_loc_zip",                   limit: 15
    t.varchar       "ez_loc_country",               limit: 50
    t.varchar       "ez_loc_region",                limit: 100
    t.varchar       "ez_loc_phone",                 limit: 30
    t.string        "ez_loc_division_name",         limit: 75
    t.boolean       "ez_loc_is_home_office",                           default: false
    t.varchar       "ez_loc_agency_name",           limit: 150
    t.boolean       "is_primary",                                      default: true,  null: false
    t.varchar       "ez_loc_agency_addr",           limit: 150
    t.varchar       "ez_loc_agency_addr2",          limit: 50
    t.varchar       "ez_loc_agency_city",           limit: 100
    t.varchar       "ez_loc_agency_state",          limit: 100
    t.varchar       "ez_loc_agency_zip",            limit: 15
    t.varchar       "ez_loc_agency_country",        limit: 50
    t.varchar       "ez_loc_agency_phone",          limit: 30
    t.varchar       "ez_loc_agency_email",          limit: 150
    t.smalldatetime "ez_createstamp",                                                  null: false
    t.smalldatetime "ez_updatestamp",                                                  null: false
    t.ss_timestamp  "ez_timestamp",                                                    null: false
  end

  create_table "Ez_Company_Location_SAVE_201406181414", id: false, force: :cascade do |t|
    t.integer       "ez_company_loc_id",            limit: 4,                          null: false
    t.integer       "ez_company_id",                limit: 4,                          null: false
    t.varchar       "ez_loc_country_default",       limit: 50
    t.varchar       "ez_loc_language_default",      limit: 50
    t.varchar       "ez_loc_message_a",             limit: 8000
    t.string        "ez_loc_message_b",             limit: 4000
    t.text_basic    "ez_loc_ts_message",            limit: 2147483647
    t.boolean       "multi_language",                                  default: false
    t.boolean       "track_activity",                                  default: false
    t.boolean       "is_ezplan_customize",                             default: false
    t.smalldatetime "ez_loc_implement_date"
    t.varchar       "ez_loc_olb_tool",              limit: 15
    t.varchar_max   "ez_olb_url",                   limit: 2147483647
    t.varchar_max   "ez_olb_enc_key",               limit: 2147483647
    t.varchar       "ez_olb_site_name",             limit: 50
    t.integer       "ez_loc_olb_client_id",         limit: 4
    t.varchar       "ez_loc_olb_language_abbr",     limit: 5
    t.varchar       "ez_loc_olb_clone_login",       limit: 50
    t.varchar       "ez_loc_olb_pwd",               limit: 50
    t.varchar       "ez_loc_profile_tool",          limit: 15
    t.varchar_max   "ez_loc_profile_url",           limit: 2147483647
    t.varchar_max   "ez_loc_profile_enc_key",       limit: 2147483647
    t.varchar       "ez_loc_profile_site_name",     limit: 50
    t.varchar       "ez_loc_profile_site_id",       limit: 15
    t.varchar       "ez_loc_profile_language_abbr", limit: 5
    t.varchar       "ez_loc_gds_name",              limit: 50
    t.varbinary     "ez_loc_gds_ipcc",              limit: 50
    t.varchar       "ez_loc_sso_key",               limit: 200
    t.varchar       "ez_loc_sso_code",              limit: 50
    t.varchar       "ez_loc_sso_supersite",         limit: 50
    t.varchar       "ez_loc_sso_subsite",           limit: 50
    t.integer       "ez_loc_sso_key_expiration",    limit: 1
    t.varchar       "ez_loc_hash_type",             limit: 5
    t.varchar       "ez_loc_abbr",                  limit: 5
    t.varchar       "ez_loc_default_flag",          limit: 50
    t.varchar       "ez_loc_second_language",       limit: 50
    t.varchar       "ez_loc_second_flag",           limit: 50
    t.varchar       "ez_loc_ip_address",            limit: 50
    t.varchar       "ez_loc_number",                limit: 50
    t.varchar       "ez_loc_after_number",          limit: 50
    t.varchar       "ez_loc_name",                  limit: 100
    t.varchar       "ez_loc_addr",                  limit: 150
    t.varchar       "ez_loc_addr2",                 limit: 50
    t.varchar       "ez_loc_city",                  limit: 100
    t.varchar       "ez_loc_state",                 limit: 100
    t.varchar       "ez_loc_zip",                   limit: 15
    t.varchar       "ez_loc_country",               limit: 50
    t.varchar       "ez_loc_region",                limit: 100
    t.varchar       "ez_loc_phone",                 limit: 30
    t.string        "ez_loc_division_name",         limit: 75
    t.boolean       "ez_loc_is_home_office",                           default: false
    t.varchar       "ez_loc_agency_name",           limit: 150
    t.boolean       "is_primary",                                      default: true,  null: false
    t.varchar       "ez_loc_agency_addr",           limit: 150
    t.varchar       "ez_loc_agency_addr2",          limit: 50
    t.varchar       "ez_loc_agency_city",           limit: 100
    t.varchar       "ez_loc_agency_state",          limit: 100
    t.varchar       "ez_loc_agency_zip",            limit: 15
    t.varchar       "ez_loc_agency_country",        limit: 50
    t.varchar       "ez_loc_agency_phone",          limit: 30
    t.varchar       "ez_loc_agency_email",          limit: 150
    t.smalldatetime "ez_createstamp",                                                  null: false
    t.smalldatetime "ez_updatestamp",                                                  null: false
    t.ss_timestamp  "ez_timestamp",                                                    null: false
  end

  create_table "Ez_Company_Location_SAVE_201407080810", id: false, force: :cascade do |t|
    t.integer       "ez_company_loc_id",            limit: 4,                          null: false
    t.integer       "ez_company_id",                limit: 4,                          null: false
    t.varchar       "ez_loc_country_default",       limit: 50
    t.varchar       "ez_loc_language_default",      limit: 50
    t.varchar       "ez_loc_message_a",             limit: 8000
    t.string        "ez_loc_message_b",             limit: 4000
    t.text_basic    "ez_loc_ts_message",            limit: 2147483647
    t.boolean       "multi_language",                                  default: false
    t.boolean       "track_activity",                                  default: false
    t.boolean       "is_ezplan_customize",                             default: false
    t.smalldatetime "ez_loc_implement_date"
    t.varchar       "ez_loc_olb_tool",              limit: 15
    t.varchar_max   "ez_olb_url",                   limit: 2147483647
    t.varchar_max   "ez_olb_enc_key",               limit: 2147483647
    t.varchar       "ez_olb_site_name",             limit: 50
    t.integer       "ez_loc_olb_client_id",         limit: 4
    t.varchar       "ez_loc_olb_language_abbr",     limit: 5
    t.varchar       "ez_loc_olb_clone_login",       limit: 50
    t.varchar       "ez_loc_olb_pwd",               limit: 50
    t.varchar       "ez_loc_profile_tool",          limit: 15
    t.varchar_max   "ez_loc_profile_url",           limit: 2147483647
    t.varchar_max   "ez_loc_profile_enc_key",       limit: 2147483647
    t.varchar       "ez_loc_profile_site_name",     limit: 50
    t.varchar       "ez_loc_profile_site_id",       limit: 15
    t.varchar       "ez_loc_profile_language_abbr", limit: 5
    t.varchar       "ez_loc_gds_name",              limit: 50
    t.varbinary     "ez_loc_gds_ipcc",              limit: 50
    t.varchar       "ez_loc_sso_key",               limit: 200
    t.varchar       "ez_loc_sso_code",              limit: 50
    t.varchar       "ez_loc_sso_supersite",         limit: 50
    t.varchar       "ez_loc_sso_subsite",           limit: 50
    t.integer       "ez_loc_sso_key_expiration",    limit: 1
    t.varchar       "ez_loc_hash_type",             limit: 5
    t.varchar       "ez_loc_abbr",                  limit: 5
    t.varchar       "ez_loc_default_flag",          limit: 50
    t.varchar       "ez_loc_second_language",       limit: 50
    t.varchar       "ez_loc_second_flag",           limit: 50
    t.varchar       "ez_loc_ip_address",            limit: 50
    t.varchar       "ez_loc_number",                limit: 50
    t.varchar       "ez_loc_after_number",          limit: 50
    t.varchar       "ez_loc_name",                  limit: 100
    t.varchar       "ez_loc_addr",                  limit: 150
    t.varchar       "ez_loc_addr2",                 limit: 50
    t.varchar       "ez_loc_city",                  limit: 100
    t.varchar       "ez_loc_state",                 limit: 100
    t.varchar       "ez_loc_zip",                   limit: 15
    t.varchar       "ez_loc_country",               limit: 50
    t.varchar       "ez_loc_region",                limit: 100
    t.varchar       "ez_loc_phone",                 limit: 30
    t.string        "ez_loc_division_name",         limit: 75
    t.boolean       "ez_loc_is_home_office",                           default: false
    t.varchar       "ez_loc_agency_name",           limit: 150
    t.boolean       "is_primary",                                      default: true,  null: false
    t.varchar       "ez_loc_agency_addr",           limit: 150
    t.varchar       "ez_loc_agency_addr2",          limit: 50
    t.varchar       "ez_loc_agency_city",           limit: 100
    t.varchar       "ez_loc_agency_state",          limit: 100
    t.varchar       "ez_loc_agency_zip",            limit: 15
    t.varchar       "ez_loc_agency_country",        limit: 50
    t.varchar       "ez_loc_agency_phone",          limit: 30
    t.varchar       "ez_loc_agency_email",          limit: 150
    t.smalldatetime "ez_createstamp",                                                  null: false
    t.smalldatetime "ez_updatestamp",                                                  null: false
    t.ss_timestamp  "ez_timestamp",                                                    null: false
  end

  create_table "Ez_Company_Location_SAVE_201407230925", id: false, force: :cascade do |t|
    t.integer       "ez_company_loc_id",            limit: 4,                          null: false
    t.integer       "ez_company_id",                limit: 4,                          null: false
    t.varchar       "ez_loc_country_default",       limit: 50
    t.varchar       "ez_loc_language_default",      limit: 50
    t.varchar       "ez_loc_message_a",             limit: 8000
    t.string        "ez_loc_message_b",             limit: 4000
    t.text_basic    "ez_loc_ts_message",            limit: 2147483647
    t.boolean       "multi_language",                                  default: false
    t.boolean       "track_activity",                                  default: false
    t.boolean       "is_ezplan_customize",                             default: false
    t.smalldatetime "ez_loc_implement_date"
    t.varchar       "ez_loc_olb_tool",              limit: 15
    t.varchar_max   "ez_olb_url",                   limit: 2147483647
    t.varchar_max   "ez_olb_enc_key",               limit: 2147483647
    t.varchar       "ez_olb_site_name",             limit: 50
    t.integer       "ez_loc_olb_client_id",         limit: 4
    t.varchar       "ez_loc_olb_language_abbr",     limit: 5
    t.varchar       "ez_loc_olb_clone_login",       limit: 50
    t.varchar       "ez_loc_olb_pwd",               limit: 50
    t.varchar       "ez_loc_profile_tool",          limit: 15
    t.varchar_max   "ez_loc_profile_url",           limit: 2147483647
    t.varchar_max   "ez_loc_profile_enc_key",       limit: 2147483647
    t.varchar       "ez_loc_profile_site_name",     limit: 50
    t.varchar       "ez_loc_profile_site_id",       limit: 15
    t.varchar       "ez_loc_profile_language_abbr", limit: 5
    t.varchar       "ez_loc_gds_name",              limit: 50
    t.varbinary     "ez_loc_gds_ipcc",              limit: 50
    t.varchar       "ez_loc_sso_key",               limit: 200
    t.varchar       "ez_loc_sso_code",              limit: 50
    t.varchar       "ez_loc_sso_supersite",         limit: 50
    t.varchar       "ez_loc_sso_subsite",           limit: 50
    t.integer       "ez_loc_sso_key_expiration",    limit: 1
    t.varchar       "ez_loc_hash_type",             limit: 5
    t.varchar       "ez_loc_abbr",                  limit: 5
    t.varchar       "ez_loc_default_flag",          limit: 50
    t.varchar       "ez_loc_second_language",       limit: 50
    t.varchar       "ez_loc_second_flag",           limit: 50
    t.varchar       "ez_loc_ip_address",            limit: 50
    t.varchar       "ez_loc_number",                limit: 50
    t.varchar       "ez_loc_after_number",          limit: 50
    t.varchar       "ez_loc_name",                  limit: 100
    t.varchar       "ez_loc_addr",                  limit: 150
    t.varchar       "ez_loc_addr2",                 limit: 50
    t.varchar       "ez_loc_city",                  limit: 100
    t.varchar       "ez_loc_state",                 limit: 100
    t.varchar       "ez_loc_zip",                   limit: 15
    t.varchar       "ez_loc_country",               limit: 50
    t.varchar       "ez_loc_region",                limit: 100
    t.varchar       "ez_loc_phone",                 limit: 30
    t.string        "ez_loc_division_name",         limit: 75
    t.boolean       "ez_loc_is_home_office",                           default: false
    t.varchar       "ez_loc_agency_name",           limit: 150
    t.boolean       "is_primary",                                      default: true,  null: false
    t.varchar       "ez_loc_agency_addr",           limit: 150
    t.varchar       "ez_loc_agency_addr2",          limit: 50
    t.varchar       "ez_loc_agency_city",           limit: 100
    t.varchar       "ez_loc_agency_state",          limit: 100
    t.varchar       "ez_loc_agency_zip",            limit: 15
    t.varchar       "ez_loc_agency_country",        limit: 50
    t.varchar       "ez_loc_agency_phone",          limit: 30
    t.varchar       "ez_loc_agency_email",          limit: 150
    t.smalldatetime "ez_createstamp",                                                  null: false
    t.smalldatetime "ez_updatestamp",                                                  null: false
    t.ss_timestamp  "ez_timestamp",                                                    null: false
  end

  create_table "Ez_Company_Location_SAVE_201407291745", id: false, force: :cascade do |t|
    t.integer       "ez_company_loc_id",            limit: 4,                          null: false
    t.integer       "ez_company_id",                limit: 4,                          null: false
    t.varchar       "ez_loc_country_default",       limit: 50
    t.varchar       "ez_loc_language_default",      limit: 50
    t.varchar       "ez_loc_message_a",             limit: 8000
    t.string        "ez_loc_message_b",             limit: 4000
    t.text_basic    "ez_loc_ts_message",            limit: 2147483647
    t.boolean       "multi_language",                                  default: false
    t.boolean       "track_activity",                                  default: false
    t.boolean       "is_ezplan_customize",                             default: false
    t.smalldatetime "ez_loc_implement_date"
    t.varchar       "ez_loc_olb_tool",              limit: 15
    t.varchar_max   "ez_olb_url",                   limit: 2147483647
    t.varchar_max   "ez_olb_enc_key",               limit: 2147483647
    t.varchar       "ez_olb_site_name",             limit: 50
    t.integer       "ez_loc_olb_client_id",         limit: 4
    t.varchar       "ez_loc_olb_language_abbr",     limit: 5
    t.varchar       "ez_loc_olb_clone_login",       limit: 50
    t.varchar       "ez_loc_olb_pwd",               limit: 50
    t.varchar       "ez_loc_profile_tool",          limit: 15
    t.varchar_max   "ez_loc_profile_url",           limit: 2147483647
    t.varchar_max   "ez_loc_profile_enc_key",       limit: 2147483647
    t.varchar       "ez_loc_profile_site_name",     limit: 50
    t.varchar       "ez_loc_profile_site_id",       limit: 15
    t.varchar       "ez_loc_profile_language_abbr", limit: 5
    t.varchar       "ez_loc_gds_name",              limit: 50
    t.varbinary     "ez_loc_gds_ipcc",              limit: 50
    t.varchar       "ez_loc_sso_key",               limit: 200
    t.varchar       "ez_loc_sso_code",              limit: 50
    t.varchar       "ez_loc_sso_supersite",         limit: 50
    t.varchar       "ez_loc_sso_subsite",           limit: 50
    t.integer       "ez_loc_sso_key_expiration",    limit: 1
    t.varchar       "ez_loc_hash_type",             limit: 5
    t.varchar       "ez_loc_abbr",                  limit: 5
    t.varchar       "ez_loc_default_flag",          limit: 50
    t.varchar       "ez_loc_second_language",       limit: 50
    t.varchar       "ez_loc_second_flag",           limit: 50
    t.varchar       "ez_loc_ip_address",            limit: 50
    t.varchar       "ez_loc_number",                limit: 50
    t.varchar       "ez_loc_after_number",          limit: 50
    t.varchar       "ez_loc_name",                  limit: 100
    t.varchar       "ez_loc_addr",                  limit: 150
    t.varchar       "ez_loc_addr2",                 limit: 50
    t.varchar       "ez_loc_city",                  limit: 100
    t.varchar       "ez_loc_state",                 limit: 100
    t.varchar       "ez_loc_zip",                   limit: 15
    t.varchar       "ez_loc_country",               limit: 50
    t.varchar       "ez_loc_region",                limit: 100
    t.varchar       "ez_loc_phone",                 limit: 30
    t.string        "ez_loc_division_name",         limit: 75
    t.boolean       "ez_loc_is_home_office",                           default: false
    t.varchar       "ez_loc_agency_name",           limit: 150
    t.boolean       "is_primary",                                      default: true,  null: false
    t.varchar       "ez_loc_agency_addr",           limit: 150
    t.varchar       "ez_loc_agency_addr2",          limit: 50
    t.varchar       "ez_loc_agency_city",           limit: 100
    t.varchar       "ez_loc_agency_state",          limit: 100
    t.varchar       "ez_loc_agency_zip",            limit: 15
    t.varchar       "ez_loc_agency_country",        limit: 50
    t.varchar       "ez_loc_agency_phone",          limit: 30
    t.varchar       "ez_loc_agency_email",          limit: 150
    t.smalldatetime "ez_createstamp",                                                  null: false
    t.smalldatetime "ez_updatestamp",                                                  null: false
    t.ss_timestamp  "ez_timestamp",                                                    null: false
  end

  create_table "Ez_Company_SAVE_201401080820", id: false, force: :cascade do |t|
    t.integer       "ez_company_id",              limit: 4,                                        null: false
    t.integer       "ez_dependency_id",           limit: 4
    t.varchar       "ez_company_name",            limit: 50,                                       null: false
    t.varchar       "ez_site_path",               limit: 150,                                      null: false
    t.varchar       "ez_gds_company_name",        limit: 50
    t.varchar       "ez_olb_company_name",        limit: 50
    t.varchar       "ez_olb_clone_login",         limit: 50
    t.varchar       "ez_company_email",           limit: 150
    t.varchar       "ez_resx_pwd",                limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",              limit: 50
    t.varchar       "ez_alias_path",              limit: 150
    t.varchar       "ez_logo_path",               limit: 100,        default: "header.jpg",        null: false
    t.varchar       "ez_primary_color",           limit: 10
    t.varchar       "ez_secondary_color",         limit: 10
    t.varchar       "ez_accent_color",            limit: 10
    t.varchar       "ez_sso_key",                 limit: 200
    t.integer       "sso_key_expiration",         limit: 1
    t.varchar       "ez_hash_type",               limit: 5
    t.text_basic    "ez_message_a",               limit: 2147483647
    t.text_basic    "ez_message_b",               limit: 2147483647
    t.varchar       "ez_ts_message",              limit: 300
    t.boolean       "is_portal",                                     default: false
    t.varchar       "portal_ver",                 limit: 20
    t.boolean       "is_implemented",                                default: false
    t.smalldatetime "implement_date"
    t.varchar       "ez_travel_department_name",  limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone", limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email", limit: 100
    t.varchar       "ez_pkey",                    limit: 200
    t.varchar       "ip_address",                 limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",   limit: 150
    t.varchar       "define_login",               limit: 50
    t.boolean       "is_global",                                     default: false
    t.boolean       "tracking_on",                                   default: false
    t.smalldatetime "ez_createstamp",                                                              null: false
    t.smalldatetime "ez_updatestamp",                                                              null: false
    t.varchar       "ez_update_by",               limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                null: false
    t.boolean       "ez_active",                                     default: true
    t.boolean       "ez_hidden",                                     default: false
  end

  create_table "Ez_Company_SAVE_201406290146", id: false, force: :cascade do |t|
    t.integer       "ez_company_id",              limit: 4,                                        null: false
    t.integer       "ez_dependency_id",           limit: 4
    t.varchar       "ez_company_name",            limit: 50,                                       null: false
    t.varchar       "ez_site_path",               limit: 150,                                      null: false
    t.varchar       "ez_gds_company_name",        limit: 50
    t.varchar       "ez_olb_company_name",        limit: 50
    t.varchar       "ez_olb_clone_login",         limit: 50
    t.varchar       "ez_company_email",           limit: 150
    t.varchar       "ez_resx_pwd",                limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",              limit: 50
    t.varchar       "ez_alias_path",              limit: 150
    t.varchar       "ez_logo_path",               limit: 100,        default: "header.jpg",        null: false
    t.varchar       "ez_primary_color",           limit: 10
    t.varchar       "ez_secondary_color",         limit: 10
    t.varchar       "ez_accent_color",            limit: 10
    t.varchar       "ez_sso_key",                 limit: 200
    t.integer       "sso_key_expiration",         limit: 1
    t.varchar       "ez_hash_type",               limit: 5
    t.text_basic    "ez_message_a",               limit: 2147483647
    t.text_basic    "ez_message_b",               limit: 2147483647
    t.varchar       "ez_ts_message",              limit: 300
    t.boolean       "is_portal",                                     default: false
    t.varchar       "portal_ver",                 limit: 20
    t.boolean       "is_implemented",                                default: false
    t.smalldatetime "implement_date"
    t.varchar       "ez_travel_department_name",  limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone", limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email", limit: 100
    t.varchar       "ez_pkey",                    limit: 200
    t.varchar       "ip_address",                 limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",   limit: 150
    t.varchar       "define_login",               limit: 50
    t.boolean       "is_global",                                     default: false
    t.boolean       "tracking_on",                                   default: false
    t.smalldatetime "ez_createstamp",                                                              null: false
    t.smalldatetime "ez_updatestamp",                                                              null: false
    t.varchar       "ez_update_by",               limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                null: false
    t.boolean       "ez_active",                                     default: true
    t.boolean       "ez_hidden",                                     default: false
  end

  create_table "Ez_Company_SAVE_201407080810", id: false, force: :cascade do |t|
    t.integer       "ez_company_id",              limit: 4,                                        null: false
    t.integer       "ez_dependency_id",           limit: 4
    t.varchar       "ez_company_name",            limit: 50,                                       null: false
    t.varchar       "ez_site_path",               limit: 150,                                      null: false
    t.varchar       "ez_gds_company_name",        limit: 50
    t.varchar       "ez_olb_company_name",        limit: 50
    t.varchar       "ez_olb_clone_login",         limit: 50
    t.varchar       "ez_company_email",           limit: 150
    t.varchar       "ez_resx_pwd",                limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",              limit: 50
    t.varchar       "ez_alias_path",              limit: 150
    t.varchar       "ez_logo_path",               limit: 100,        default: "header.jpg",        null: false
    t.varchar       "ez_primary_color",           limit: 10
    t.varchar       "ez_secondary_color",         limit: 10
    t.varchar       "ez_accent_color",            limit: 10
    t.varchar       "ez_sso_key",                 limit: 200
    t.integer       "sso_key_expiration",         limit: 1
    t.varchar       "ez_hash_type",               limit: 5
    t.text_basic    "ez_message_a",               limit: 2147483647
    t.text_basic    "ez_message_b",               limit: 2147483647
    t.varchar       "ez_ts_message",              limit: 300
    t.boolean       "is_portal",                                     default: false
    t.varchar       "portal_ver",                 limit: 20
    t.boolean       "is_implemented",                                default: false
    t.smalldatetime "implement_date"
    t.boolean       "is_in_use",                                     default: true,                null: false
    t.varchar       "ez_travel_department_name",  limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone", limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email", limit: 100
    t.varchar       "ez_pkey",                    limit: 200
    t.varchar       "ip_address",                 limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",   limit: 150
    t.varchar       "define_login",               limit: 50
    t.boolean       "is_global",                                     default: false
    t.boolean       "tracking_on",                                   default: false
    t.smalldatetime "ez_createstamp",                                                              null: false
    t.smalldatetime "ez_updatestamp",                                                              null: false
    t.varchar       "ez_update_by",               limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                null: false
    t.boolean       "ez_active",                                     default: true
    t.boolean       "ez_hidden",                                     default: false
  end

  create_table "Ez_Company_SAVE_201410171019", id: false, force: :cascade do |t|
    t.integer       "ez_company_id",              limit: 4,                                        null: false
    t.integer       "ez_dependency_id",           limit: 4
    t.varchar       "ez_company_name",            limit: 50,                                       null: false
    t.varchar       "ez_site_path",               limit: 150,                                      null: false
    t.varchar       "ez_gds_company_name",        limit: 50
    t.varchar       "ez_olb_company_name",        limit: 50
    t.varchar       "ez_olb_clone_login",         limit: 50
    t.varchar       "ez_company_email",           limit: 150
    t.varchar       "ez_resx_pwd",                limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",              limit: 50
    t.varchar       "ez_alias_path",              limit: 150
    t.varchar       "ez_logo_path",               limit: 100,        default: "header.jpg",        null: false
    t.varchar       "ez_primary_color",           limit: 10
    t.varchar       "ez_secondary_color",         limit: 10
    t.varchar       "ez_accent_color",            limit: 10
    t.varchar       "ez_sso_key",                 limit: 200
    t.integer       "sso_key_expiration",         limit: 1
    t.varchar       "ez_hash_type",               limit: 5
    t.text_basic    "ez_message_a",               limit: 2147483647
    t.text_basic    "ez_message_b",               limit: 2147483647
    t.varchar       "ez_ts_message",              limit: 300
    t.boolean       "is_portal",                                     default: false
    t.varchar       "portal_ver",                 limit: 20
    t.boolean       "is_implemented",                                default: false
    t.smalldatetime "implement_date"
    t.boolean       "is_in_use",                                     default: true,                null: false
    t.varchar       "ez_travel_department_name",  limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone", limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email", limit: 100
    t.varchar       "ez_pkey",                    limit: 200
    t.varchar       "ip_address",                 limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",   limit: 150
    t.varchar       "define_login",               limit: 50
    t.boolean       "is_global",                                     default: false
    t.boolean       "tracking_on",                                   default: false
    t.smalldatetime "ez_createstamp",                                                              null: false
    t.smalldatetime "ez_updatestamp",                                                              null: false
    t.varchar       "ez_update_by",               limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                null: false
    t.boolean       "ez_active",                                     default: true
    t.boolean       "ez_hidden",                                     default: false
  end

  create_table "Ez_Company_Sso_Key", primary_key: "ez_company_sso_key_id", force: :cascade do |t|
    t.integer       "ez_company_id",  limit: 4,   null: false
    t.integer       "ez_product_id",  limit: 4,   null: false
    t.varchar       "ez_sso_key",     limit: 128, null: false
    t.varchar       "ez_logo_path",   limit: 100
    t.smalldatetime "ez_createstamp",             null: false
    t.smalldatetime "ez_updatestamp",             null: false
    t.ss_timestamp  "ez_timestamp",               null: false
  end

  create_table "Ez_Company_WORK_201406290149", id: false, force: :cascade do |t|
    t.integer       "ez_company_id",              limit: 4,                                        null: false
    t.integer       "ez_dependency_id",           limit: 4
    t.varchar       "ez_company_name",            limit: 50,                                       null: false
    t.varchar       "ez_site_path",               limit: 150,                                      null: false
    t.varchar       "ez_gds_company_name",        limit: 50
    t.varchar       "ez_olb_company_name",        limit: 50
    t.varchar       "ez_olb_clone_login",         limit: 50
    t.varchar       "ez_company_email",           limit: 150
    t.varchar       "ez_resx_pwd",                limit: 50,         default: "travel1"
    t.varchar       "ez_cytric_pwd",              limit: 50
    t.varchar       "ez_alias_path",              limit: 150
    t.varchar       "ez_logo_path",               limit: 100,        default: "header.jpg",        null: false
    t.varchar       "ez_primary_color",           limit: 10
    t.varchar       "ez_secondary_color",         limit: 10
    t.varchar       "ez_accent_color",            limit: 10
    t.varchar       "ez_sso_key",                 limit: 200
    t.integer       "sso_key_expiration",         limit: 1
    t.varchar       "ez_hash_type",               limit: 5
    t.text_basic    "ez_message_a",               limit: 2147483647
    t.text_basic    "ez_message_b",               limit: 2147483647
    t.varchar       "ez_ts_message",              limit: 300
    t.boolean       "is_portal",                                     default: false
    t.varchar       "portal_ver",                 limit: 20
    t.boolean       "is_implemented",                                default: false
    t.smalldatetime "implement_date"
    t.boolean       "is_in_use",                                     default: true,                null: false
    t.varchar       "ez_travel_department_name",  limit: 100,        default: "Travel Department"
    t.varchar       "ez_travel_department_phone", limit: 20,         default: "614-901-4100"
    t.varchar       "ez_travel_department_email", limit: 100
    t.varchar       "ez_pkey",                    limit: 200
    t.varchar       "ip_address",                 limit: 50
    t.boolean       "custom_login"
    t.varchar       "custom_login_description",   limit: 150
    t.varchar       "define_login",               limit: 50
    t.boolean       "is_global",                                     default: false
    t.boolean       "tracking_on",                                   default: false
    t.smalldatetime "ez_createstamp",                                                              null: false
    t.smalldatetime "ez_updatestamp",                                                              null: false
    t.varchar       "ez_update_by",               limit: 100
    t.ss_timestamp  "ez_timestamp",                                                                null: false
    t.boolean       "ez_active",                                     default: true
    t.boolean       "ez_hidden",                                     default: false
  end

  create_table "Ez_Content", primary_key: "ez_content_id", force: :cascade do |t|
    t.varchar       "content_label",   limit: 50,                         null: false
    t.text_basic    "content",         limit: 2147483647,                 null: false
    t.char          "content_pos",     limit: 3,                          null: false
    t.char          "content_version", limit: 1,          default: "1",   null: false
    t.char          "stat",            limit: 1,          default: "A",   null: false
    t.integer       "ez_company_id",   limit: 4,                          null: false
    t.integer       "ez_event_id",     limit: 4,                          null: false
    t.integer       "ez_page_id",      limit: 4,                          null: false
    t.smalldatetime "ez_createstamp",                                     null: false
    t.smalldatetime "ez_updatestamp",                                     null: false
    t.binary_basic  "ez_timestamp",    limit: 8,                          null: false
    t.boolean       "ez_active",                          default: false, null: false
  end

  create_table "Ez_Cruise", primary_key: "ez_cruise_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                             null: false
    t.integer       "ez_event_id",                   limit: 4,                             null: false
    t.integer       "ez_company_id",                 limit: 4,                             null: false
    t.varchar       "cruise_required",               limit: 10
    t.varchar       "ez_bk_agent_code",              limit: 3
    t.varchar       "ez_tk_agent_code",              limit: 3
    t.integer       "ez_dependency_id",              limit: 4
    t.varchar       "ez_dk",                         limit: 50
    t.integer       "cruise_num_port",               limit: 4,                             null: false
    t.boolean       "cruise_port_connection",                           default: false,    null: false
    t.varchar       "cruise_trip",                   limit: 15,                            null: false
    t.varchar       "cruise_trip_name",              limit: 50
    t.char          "cruise_trip_type",              limit: 1,          default: "D"
    t.varchar       "cruise_departing_port",         limit: 100
    t.varchar       "port_departure_city",           limit: 50,                            null: false
    t.varchar       "port_departure_state",          limit: 50,                            null: false
    t.smalldatetime "port_departure_date",                                                 null: false
    t.varchar       "port_departure_time",           limit: 50
    t.varchar       "port_departure_by",             limit: 50
    t.varchar       "cruise_arrival_port",           limit: 100
    t.varchar       "port_arrival_city",             limit: 50
    t.varchar       "port_arrival_state",            limit: 50
    t.smalldatetime "port_arrival_date"
    t.varchar       "port_arrival_time",             limit: 50
    t.varchar       "port_arrival_by",               limit: 50
    t.text_basic    "cruise_special_request",        limit: 2147483647
    t.varchar       "celebrating_an_occasion",       limit: 250
    t.varchar       "cruise_cabin",                  limit: 50
    t.varchar       "cruise_class",                  limit: 50
    t.boolean       "cruise_cabin_window",                              default: false,    null: false
    t.varchar       "cruise_deck",                   limit: 50
    t.varchar       "cruise_vendor",                 limit: 100
    t.varchar       "cruise_ship_name",              limit: 100
    t.varchar       "cruise_travel_region",          limit: 50
    t.varchar       "cruise_confirmation",           limit: 50
    t.boolean       "agency_book_air",                                  default: false,    null: false
    t.boolean       "agency_book_hotel",                                default: false,    null: false
    t.boolean       "air_deviation",                                    default: false,    null: false
    t.boolean       "pre_hotel_package",                                default: false,    null: false
    t.boolean       "post_hotel_package",                               default: false,    null: false
    t.boolean       "pre_air_package",                                  default: false,    null: false
    t.boolean       "post_air_package",                                 default: false,    null: false
    t.boolean       "airport_transportation",                           default: false,    null: false
    t.boolean       "take_insurance",                                   default: false,    null: false
    t.boolean       "extra_package",                                                       null: false
    t.varchar       "excursion1",                    limit: 100
    t.varchar       "excursion2",                    limit: 100
    t.varchar       "excursion3",                    limit: 100
    t.varchar       "dining_time",                   limit: 15
    t.varchar       "dining_table_size",             limit: 50
    t.boolean       "have_passport",                                    default: false,    null: false
    t.char          "num_people_per_cabin",          limit: 1,          default: "1"
    t.char          "num_of_cabin",                  limit: 1,          default: "1"
    t.varchar       "gds_record_locator",            limit: 15
    t.float         "cruise_discount"
    t.float         "cruise_tax"
    t.float         "cruise_ttl_cost"
    t.varchar       "cruise_direct_bill",            limit: 50
    t.varchar       "cruise_billing_code",           limit: 100
    t.varchar       "cruise_account_code",           limit: 50
    t.char          "cruise_budgeted",               limit: 3
    t.float         "cruise_budget_amt"
    t.varchar       "cruise_payment_status",         limit: 15
    t.varchar       "cruise_invoice_num",            limit: 20
    t.float         "payment_deposite1"
    t.smalldatetime "payment_deposite1_date"
    t.float         "payment_deposite2"
    t.smalldatetime "payment_deposite2_date"
    t.float         "payment_deposite3"
    t.smalldatetime "payment_deposite3_date"
    t.float         "final_payment"
    t.smalldatetime "final_payment_date"
    t.varchar       "vendor_locator",                limit: 15
    t.float         "service_fee"
    t.varchar       "custom_field1",                 limit: 100
    t.varchar       "custom_field2",                 limit: 100
    t.varchar       "custom_field3",                 limit: 100
    t.varchar       "custom_field4",                 limit: 100
    t.varchar       "custom_field5",                 limit: 100
    t.boolean       "ez_non_refundable",                                default: false
    t.boolean       "ez_exchange",                                      default: false
    t.boolean       "ez_mco",                                           default: false
    t.boolean       "ez_void",                                          default: false
    t.boolean       "ez_error",                                         default: false
    t.varchar       "ez_event_authentication_value", limit: 128,                           null: false
    t.smalldatetime "ez_createstamp",                                                      null: false
    t.smalldatetime "ez_updatestamp",                                                      null: false
    t.ss_timestamp  "ez_timestamp",                                                        null: false
    t.boolean       "ez_active",                                        default: true
    t.boolean       "ez_hidden",                                        default: false
    t.boolean       "ez_save_trip",                                     default: false
    t.varchar       "ez_rec_type",                   limit: 15,         default: "ONLINE"
  end

  create_table "Ez_Dk", primary_key: "ez_dk_id", force: :cascade do |t|
    t.integer       "ez_company_id",      limit: 4,                          null: false
    t.string        "ez_pseudo",          limit: 50
    t.string        "ez_dp_company_name", limit: 50
    t.text          "ez_dk",              limit: 2147483647
    t.smalldatetime "ez_createstamp",                                        null: false
    t.smalldatetime "ez_updatestamp",                                        null: false
    t.ss_timestamp  "ez_timestamp",                                          null: false
    t.boolean       "ez_active",                             default: true
    t.boolean       "ez_hidden",                             default: false
  end

  create_table "Ez_Eblast", primary_key: "ez_eblast_id", force: :cascade do |t|
    t.integer       "ez_event_id",            limit: 4
    t.integer       "ez_company_id",          limit: 4
    t.integer       "ez_traveler_profile_id", limit: 4
    t.char          "ez_air_id",              limit: 10
    t.char          "ez_udid_id",             limit: 10
    t.char          "ez_human_error_id",      limit: 10
    t.smalldatetime "sent_date",                                          null: false
    t.smalldatetime "last_sent_date",                                     null: false
    t.varchar       "sent_email",             limit: 100,                 null: false
    t.boolean       "respond"
    t.char          "respond_type",           limit: 3
    t.boolean       "remove_email",                       default: false, null: false
    t.varchar       "remove_remark",          limit: 50
    t.smalldatetime "ez_createstamp",                                     null: false
    t.smalldatetime "ez_updatestamp",                                     null: false
    t.ss_timestamp  "ez_timestamp",                                       null: false
  end

  create_table "Ez_Edit_Board", id: false, force: :cascade do |t|
    t.integer       "ez_edit_board_id",                 limit: 4,                   null: false
    t.integer       "ez_company_id",                    limit: 4,                   null: false
    t.integer       "ez_edit_for_event_id",             limit: 4,                   null: false
    t.integer       "ez_message_for_user_profile_id",   limit: 4,   default: 10000, null: false
    t.integer       "ez_message_taker_user_profile_id", limit: 4,                   null: false
    t.integer       "ez_dependency_id",                 limit: 4,                   null: false
    t.integer       "ez_agent_id",                      limit: 4,                   null: false
    t.varchar       "ez_message",                       limit: 250,                 null: false
    t.varchar       "ez_description",                   limit: 100
    t.varchar       "ez_type",                          limit: 5
    t.varchar       "ez_status",                        limit: 5,                   null: false
    t.float         "ez_cost_avoidance"
    t.float         "ez_cost_savings"
    t.smalldatetime "ez_createstamp",                                               null: false
    t.smalldatetime "ez_updatestamp",                                               null: false
    t.ss_timestamp  "ez_timestamp",                                                 null: false
    t.boolean       "ez_active",                                    default: false, null: false
  end

  create_table "Ez_Employee_Project", primary_key: "ez_employee_project_id", force: :cascade do |t|
    t.integer  "ez_user_id",    limit: 4, null: false
    t.integer  "ez_project_id", limit: 4
    t.integer  "ez_issue_id",   limit: 4
    t.datetime "deletedat"
  end

  create_table "Ez_Error", primary_key: "ez_error_id", force: :cascade do |t|
    t.integer       "ez_event_id",                 limit: 4,                   null: false
    t.integer       "ez_company_id",               limit: 4,                   null: false
    t.integer       "ez_user_profile_id",          limit: 4,                   null: false
    t.integer       "ez_agent_id",                 limit: 4,                   null: false
    t.varchar       "ez_event_authentication_key", limit: 30,                  null: false
    t.char          "record_locator",              limit: 10,                  null: false
    t.varchar       "error_type",                  limit: 50
    t.varchar       "error_code",                  limit: 100
    t.varchar       "error_value",                 limit: 100
    t.varchar       "error_status",                limit: 50
    t.varchar       "error_name",                  limit: 50
    t.varchar       "error_notifier_email",        limit: 100
    t.varchar       "eror_remarks",                limit: 50
    t.smalldatetime "ez_createstamp",                                          null: false
    t.ss_timestamp  "ez_timestamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                          null: false
    t.boolean       "ez_active",                               default: false, null: false
  end

  create_table "Ez_Event", primary_key: "ez_event_id", force: :cascade do |t|
    t.integer       "ez_company_id",                 limit: 4,                          null: false
    t.integer       "ez_country_id",                 limit: 2
    t.integer       "ez_event_type_id",              limit: 4,                          null: false
    t.integer       "ez_product_id",                 limit: 4
    t.integer       "ez_status_id",                  limit: 4
    t.varchar       "ez_dk",                         limit: 50
    t.varchar       "ez_event_gds_name",             limit: 50
    t.varchar       "ez_application_path",           limit: 200
    t.varchar       "ez_event_name",                 limit: 200,                        null: false
    t.varchar       "ez_event_code",                 limit: 50
    t.text_basic    "ez_event_description",          limit: 2147483647
    t.varchar       "clone_from_site",               limit: 150
    t.boolean       "need_ez_work",                                     default: false, null: false
    t.varchar       "ez_event_mtgnumber",            limit: 50
    t.varchar       "client_meeting_number",         limit: 15
    t.varchar       "ts_planner_name",               limit: 100
    t.varchar       "ts_planner_email",              limit: 150
    t.varchar       "ez_meeting_contact_name",       limit: 150
    t.varchar       "ez_contact_email",              limit: 150
    t.varchar       "ez_contact_number",             limit: 30
    t.varchar       "ez_contact_ext",                limit: 5
    t.varchar       "ez_contact_fax",                limit: 30
    t.varchar       "ez_department_contact_name",    limit: 150
    t.integer       "ez_group_size",                 limit: 2
    t.float         "ez_group_budget"
    t.varchar       "ez_event_cost_center",          limit: 50
    t.smalldatetime "ez_event_arrival_date",                                            null: false
    t.smalldatetime "ez_event_return_date",                                             null: false
    t.smalldatetime "ez_event_start_date"
    t.smalldatetime "ez_event_end_date"
    t.varchar       "ez_event_location",             limit: 250
    t.varchar       "ez_event_venue",                limit: 150
    t.varchar       "ez_event_latitude_longtitude",  limit: 50
    t.varchar       "ez_event_zip",                  limit: 15
    t.char          "ez_event_airport_code",         limit: 3
    t.char          "ez_event_state",                limit: 2
    t.smalldatetime "ez_event_open_reg_date"
    t.smalldatetime "ez_event_close_reg_date"
    t.smalldatetime "ez_event_display_close_date"
    t.varchar       "ez_event_session_name",         limit: 50
    t.varchar       "ez_event_general_email",        limit: 100
    t.varchar       "ez_site_close_page",            limit: 100
    t.text_basic    "ez_site_close_message",         limit: 2147483647
    t.varchar       "event_column_identifier",       limit: 50
    t.varchar       "event_column_number",           limit: 10
    t.varchar       "ez_event_authentication_value", limit: 128,                        null: false
    t.varchar       "ts_site_message",               limit: 1000
    t.varchar       "publish_code",                  limit: 5
    t.varchar       "color_hex_code",                limit: 7
    t.boolean       "is_air_optional"
    t.boolean       "is_hotel_optional"
    t.boolean       "is_car_optional"
    t.boolean       "is_activity_optional"
    t.boolean       "is_payment_needed"
    t.varchar       "event_contact_number",          limit: 30
    t.boolean       "is_new",                                           default: true
    t.smalldatetime "ez_close_process"
    t.smalldatetime "ez_createstamp",                                                   null: false
    t.smalldatetime "ez_updatestamp",                                                   null: false
    t.ss_timestamp  "ez_timestamp",                                                     null: false
    t.boolean       "ez_active",                                        default: true,  null: false
    t.boolean       "ez_hidden",                                        default: false, null: false
  end

  create_table "Ez_Event_Contact", primary_key: "ez_event_contact_id", force: :cascade do |t|
    t.integer "ez_event_id",         limit: 4,   null: false
    t.varchar "event_contact_lname", limit: 150
    t.varchar "event_contact_fname", limit: 150
    t.varchar "event_contact_email", limit: 150
    t.varchar "event_contact_phone", limit: 30
    t.varchar "event_contact_ext",   limit: 5
    t.varchar "event_contact_cell",  limit: 30
    t.varchar "event_contact_fax",   limit: 30
    t.varchar "event_contact_alt",   limit: 30
    t.varchar "event_contact_type",  limit: 15
  end

  create_table "Ez_Event_Detail", primary_key: "ez_event_detail_id", force: :cascade do |t|
    t.integer       "ez_event_id",                        limit: 4,                                                                   null: false
    t.integer       "ez_company_id",                      limit: 4,                                                                   null: false
    t.varchar       "ez_tour_leader_name",                limit: 100
    t.smalldatetime "ez_actual_open_date"
    t.smalldatetime "ez_actual_close_date"
    t.decimal       "ez_budget",                                             precision: 2, scale: 0
    t.varchar       "ez_bank_num",                        limit: 50
    t.varchar       "ez_division",                        limit: 100
    t.varchar       "ez_cost_center",                     limit: 25
    t.varchar       "ez_gl",                              limit: 50
    t.varchar       "ez_line_business",                   limit: 50
    t.varchar       "ez_department",                      limit: 50
    t.varchar       "on_site_host_name",                  limit: 250
    t.varchar       "on_site_phone",                      limit: 30
    t.varchar       "on_site_email",                      limit: 250
    t.boolean       "on_site_registration",                                                          default: false
    t.varchar       "additional_info_in_itin",            limit: 250
    t.smalldatetime "received_manifest_date"
    t.varchar       "meeting_host_email",                 limit: 150
    t.boolean       "itin_send_to_meeting_host",                                                     default: true
    t.boolean       "itin_send_to_attendees",                                                        default: false
    t.boolean       "itin_send_to_group_email",                                                      default: false
    t.boolean       "ticket_charge_to_bta"
    t.boolean       "ticket_charge_to_individual_card"
    t.boolean       "no_indi_card_charge_to_cost_center"
    t.boolean       "arrival_departure_list",                                                        default: false
    t.smalldatetime "arrival_departure_send_date"
    t.boolean       "after_ticked_allow_changes",                                                    default: false
    t.varchar       "changes_autherizor",                 limit: 150
    t.varchar       "deadline_for_changes",               limit: 250
    t.varchar       "cost_cap_for_changes",               limit: 250
    t.text_basic    "comments",                           limit: 2147483647
    t.varchar       "client_group_email",                 limit: 150
    t.varchar       "host_hotel1",                        limit: 150
    t.integer       "host_hotel_id1",                     limit: 4
    t.varchar       "host_hotel2",                        limit: 150
    t.integer       "host_hotel_id2",                     limit: 4
    t.varchar       "host_hotel3",                        limit: 150
    t.integer       "host_hotel_id3",                     limit: 4
    t.integer       "num_attendees",                      limit: 2,                                  default: 1
    t.char          "ez_guest_allow",                     limit: 1,                                  default: "N"
    t.integer       "num_guest",                          limit: 2
    t.integer       "num_private_guest",                  limit: 2
    t.char          "ez_guest_expense_covered",           limit: 1,                                  default: "N"
    t.decimal       "ez_setup_cost",                                         precision: 2, scale: 0
    t.char          "ez_deviation",                       limit: 1,                                  default: "N"
    t.char          "ez_transportation_required",         limit: 1,                                  default: "N"
    t.varchar       "ez_transportation_remark",           limit: 250
    t.char          "ez_dates_flexible",                  limit: 1,                                  default: "N"
    t.varchar       "ez_flexible_remark",                 limit: 250
    t.integer       "ez_num_room",                        limit: 2,                                  default: 0
    t.char          "ez_breakout_room",                   limit: 1,                                  default: "N"
    t.char          "ez_breakout_food",                   limit: 1,                                  default: "N"
    t.decimal       "ez_cost_per_transaction",                               precision: 2, scale: 0
    t.decimal       "ez_hourly_rate",                                        precision: 2, scale: 0
    t.char          "ez_custom_report",                   limit: 1,                                  default: "N"
    t.integer       "ez_event_planner_id",                limit: 4
    t.integer       "ez_event_requestor_id",              limit: 4
    t.varchar       "ez_event_logo",                      limit: 100,                                default: "default.jpg"
    t.varchar       "ez_event_app_name",                  limit: 50,                                 default: "index.cfm"
    t.varchar       "ez_event_css_path",                  limit: 100,                                default: "style/stylesheet.css"
    t.varchar       "ez_event_main_message",              limit: 250
    t.integer       "ez_policy_content_id",               limit: 4
    t.smalldatetime "ez_approved"
    t.varchar       "ez_event_authentication_id",         limit: 20,                                                                  null: false
    t.smalldatetime "ez_createstamp",                                                                                                 null: false
    t.smalldatetime "ez_updatestamp",                                                                                                 null: false
    t.ss_timestamp  "ez_timestamp",                                                                                                   null: false
    t.boolean       "ez_active",                                                                     default: true,                   null: false
    t.boolean       "ez_hidden",                                                                     default: false,                  null: false
  end

  create_table "Ez_Event_Item_Cost", primary_key: "ez_event_item_cost_id", force: :cascade do |t|
    t.integer       "ez_event_id",            limit: 4,                                           null: false
    t.varchar       "from_vendor",            limit: 100
    t.char          "contacted_vendor",       limit: 1
    t.varchar       "item_name",              limit: 50,                                          null: false
    t.decimal       "item_base_cost",                     precision: 2, scale: 0, default: 0.0,   null: false
    t.decimal       "item_base_cost_tax",                 precision: 2, scale: 0, default: 0.0,   null: false
    t.decimal       "item_discount_cost",                 precision: 2, scale: 0, default: 0.0,   null: false
    t.decimal       "item_discount_cost_tax",             precision: 2, scale: 0, default: 0.0,   null: false
    t.char          "item_is_mwbenc",         limit: 3,                           default: "N  ", null: false
    t.integer       "item_total_requested",   limit: 2,                           default: 0,     null: false
    t.decimal       "item_total_cost",                    precision: 2, scale: 0, default: 0.0,   null: false
    t.varchar       "item_remark",            limit: 250
    t.smalldatetime "item_createstamp",                                                           null: false
    t.smalldatetime "item_updatestamp",                                                           null: false
    t.boolean       "ez_active",                                                  default: false, null: false
    t.boolean       "ez_hidden",                                                  default: false, null: false
  end

  create_table "Ez_Event_Loc_Bridge", primary_key: "ez_event_loc_bridge_id", force: :cascade do |t|
    t.integer       "ez_user_id",           limit: 4,                 null: false
    t.integer       "ez_event_location_id", limit: 4,                 null: false
    t.smalldatetime "ez_createstamp",                                 null: false
    t.smalldatetime "ez_updatestamp",                                 null: false
    t.ss_timestamp  "ez_timestamp",                                   null: false
    t.boolean       "ez_active",                      default: false, null: false
    t.boolean       "ez_hidden",                      default: true,  null: false
  end

  create_table "Ez_Event_Location", primary_key: "ez_event_location_id", force: :cascade do |t|
    t.varchar       "ez_type",                  limit: 10,                                          null: false
    t.integer       "ez_event_id",              limit: 4,                                           null: false
    t.integer       "ez_company_id",            limit: 4,                                           null: false
    t.varchar       "location_name",            limit: 100,                                         null: false
    t.varchar       "address1",                 limit: 255
    t.varchar       "address2",                 limit: 255
    t.varchar       "city",                     limit: 100
    t.varchar       "state",                    limit: 100
    t.varchar       "zip",                      limit: 15
    t.varchar       "mailcode",                 limit: 50
    t.varchar       "country",                  limit: 50
    t.integer       "location_num_room",        limit: 2
    t.integer       "location_num_nights",      limit: 2
    t.integer       "location_comp_room",       limit: 2
    t.integer       "location_preferred_order", limit: 1,                           default: 1
    t.char          "location_code",            limit: 3
    t.varchar       "location_url",             limit: 100
    t.varchar       "location_image_path",      limit: 100
    t.decimal       "location_rate",                        precision: 2, scale: 0
    t.decimal       "location_fee",                         precision: 2, scale: 0
    t.smalldatetime "location_res_open"
    t.smalldatetime "location_res_close"
    t.smalldatetime "location_checkin"
    t.smalldatetime "location_checkout"
    t.integer       "policy_content_id",        limit: 4
    t.char          "location_breakfast",       limit: 1,                           default: "N"
    t.char          "location_lunch",           limit: 1,                           default: "N"
    t.char          "location_dinner",          limit: 1
    t.varchar       "location_contact",         limit: 50
    t.varchar       "location_phone",           limit: 50
    t.integer       "ez_address_id",            limit: 4
    t.varchar       "location_remark",          limit: 250
    t.smalldatetime "location_createstamp",                                                         null: false
    t.smalldatetime "location_updatestamp",                                                         null: false
    t.boolean       "ez_active",                                                    default: true,  null: false
    t.boolean       "ez_hidden",                                                    default: false, null: false
  end

  create_table "Ez_Event_Location_Detail", primary_key: "ez_event_location_detail_id", force: :cascade do |t|
    t.integer       "ez_event_location_id",        limit: 4,                                           null: false
    t.integer       "ez_address_id",               limit: 4,                                           null: false
    t.varchar       "location_detail_item",        limit: 100
    t.decimal       "location_detail_cost",                    precision: 2, scale: 0
    t.varchar       "location_detail_remark",      limit: 250
    t.smalldatetime "location_detail_createstamp",                                                     null: false
    t.smalldatetime "location_detail_updatestamp",                                                     null: false
    t.ss_timestamp  "location_detail_timestamp",                                                       null: false
    t.boolean       "ez_active",                                                       default: false, null: false
    t.boolean       "ez_hidden",                                                       default: false, null: false
  end

  create_table "Ez_Event_Product_Bridge", primary_key: "ez_event_product_bridge_id", force: :cascade do |t|
    t.integer       "ez_event_id",                   limit: 4,                   null: false
    t.integer       "ez_product_id",                 limit: 4,                   null: false
    t.integer       "ez_company_id",                 limit: 4,                   null: false
    t.varchar       "ez_event_authentication_value", limit: 128,                 null: false
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_Event_SAVE_201412111227", id: false, force: :cascade do |t|
    t.integer       "ez_event_id",                   limit: 4,                          null: false
    t.integer       "ez_company_id",                 limit: 4,                          null: false
    t.integer       "ez_country_id",                 limit: 2
    t.integer       "ez_event_type_id",              limit: 4,                          null: false
    t.integer       "ez_product_id",                 limit: 4
    t.integer       "ez_status_id",                  limit: 4
    t.varchar       "ez_dk",                         limit: 50
    t.varchar       "ez_event_gds_name",             limit: 50
    t.varchar       "ez_application_path",           limit: 200
    t.varchar       "ez_event_name",                 limit: 200,                        null: false
    t.varchar       "ez_event_code",                 limit: 50
    t.text_basic    "ez_event_description",          limit: 2147483647
    t.varchar       "clone_from_site",               limit: 150
    t.boolean       "need_ez_work",                                     default: false, null: false
    t.varchar       "ez_event_mtgnumber",            limit: 50
    t.varchar       "client_meeting_number",         limit: 15
    t.varchar       "ts_planner_name",               limit: 100
    t.varchar       "ts_planner_email",              limit: 150
    t.varchar       "ez_meeting_contact_name",       limit: 150
    t.varchar       "ez_contact_email",              limit: 150
    t.varchar       "ez_contact_number",             limit: 30
    t.varchar       "ez_contact_ext",                limit: 5
    t.varchar       "ez_contact_fax",                limit: 30
    t.varchar       "ez_department_contact_name",    limit: 150
    t.integer       "ez_group_size",                 limit: 2
    t.float         "ez_group_budget"
    t.varchar       "ez_event_cost_center",          limit: 50
    t.smalldatetime "ez_event_arrival_date",                                            null: false
    t.smalldatetime "ez_event_return_date",                                             null: false
    t.smalldatetime "ez_event_start_date"
    t.smalldatetime "ez_event_end_date"
    t.varchar       "ez_event_location",             limit: 250
    t.varchar       "ez_event_venue",                limit: 150
    t.varchar       "ez_event_latitude_longtitude",  limit: 50
    t.varchar       "ez_event_zip",                  limit: 15
    t.char          "ez_event_airport_code",         limit: 3
    t.char          "ez_event_state",                limit: 2
    t.smalldatetime "ez_event_open_reg_date"
    t.smalldatetime "ez_event_close_reg_date"
    t.smalldatetime "ez_event_display_close_date"
    t.varchar       "ez_event_session_name",         limit: 50
    t.varchar       "ez_event_general_email",        limit: 100
    t.varchar       "ez_site_close_page",            limit: 100
    t.text_basic    "ez_site_close_message",         limit: 2147483647
    t.varchar       "event_column_identifier",       limit: 50
    t.varchar       "event_column_number",           limit: 10
    t.varchar       "ez_event_authentication_value", limit: 128,                        null: false
    t.varchar       "ts_site_message",               limit: 1000
    t.varchar       "publish_code",                  limit: 5
    t.varchar       "color_hex_code",                limit: 7
    t.boolean       "is_air_optional"
    t.boolean       "is_hotel_optional"
    t.boolean       "is_car_optional"
    t.boolean       "is_activity_optional"
    t.boolean       "is_payment_needed"
    t.varchar       "event_contact_number",          limit: 30
    t.boolean       "is_new",                                           default: true
    t.smalldatetime "ez_close_process"
    t.smalldatetime "ez_createstamp",                                                   null: false
    t.smalldatetime "ez_updatestamp",                                                   null: false
    t.ss_timestamp  "ez_timestamp",                                                     null: false
    t.boolean       "ez_active",                                        default: true,  null: false
    t.boolean       "ez_hidden",                                        default: false, null: false
  end

  create_table "Ez_Event_Type", primary_key: "ez_event_type_id", force: :cascade do |t|
    t.varchar       "ez_type_name",    limit: 50, null: false
    t.char          "ez_publish_code", limit: 5
    t.smalldatetime "ez_createstamp",             null: false
    t.smalldatetime "ez_updatestamp",             null: false
    t.ss_timestamp  "ez_timestamp",               null: false
  end

  create_table "Ez_Excel_Process", primary_key: "ez_excel_process_id", force: :cascade do |t|
    t.integer       "ez_event_id",         limit: 4,                   null: false
    t.integer       "ez_user_id",          limit: 4
    t.varchar       "process_description", limit: 250,                 null: false
    t.smalldatetime "process_date",                                    null: false
    t.varchar       "process_file",        limit: 100,                 null: false
    t.integer       "num_of_max_column",   limit: 2,                   null: false
    t.integer       "num_of_row",          limit: 2,                   null: false
    t.smalldatetime "ez_createstamp",                                  null: false
    t.smalldatetime "ez_updatestamp",                                  null: false
    t.boolean       "ez_active",                       default: true,  null: false
    t.boolean       "ez_hidden",                       default: false, null: false
  end

  create_table "Ez_Exchanges", primary_key: "ez_exchange_id", force: :cascade do |t|
    t.integer       "ez_air_id",           limit: 4,                   null: false
    t.integer       "ez_event_id",         limit: 4,                   null: false
    t.integer       "ez_agent_id",         limit: 4,                   null: false
    t.varchar       "air_ticket_num",      limit: 20,                  null: false
    t.float         "air_ttl_cost",                                    null: false
    t.varchar       "record_locator",      limit: 15,                  null: false
    t.varchar       "exchange_ticket_num", limit: 20,                  null: false
    t.float         "exchange_ttl_cost",                               null: false
    t.float         "exchange_cost",                                   null: false
    t.float         "exchange_fee",                                    null: false
    t.varchar       "ez_exchange_remarks", limit: 100
    t.smalldatetime "ez_createstamp",                                  null: false
    t.smalldatetime "ez_updatestamp",                                  null: false
    t.ss_timestamp  "ez_timestamp",                                    null: false
    t.boolean       "ez_active",                       default: true,  null: false
    t.boolean       "ez_hidden",                       default: false, null: false
  end

  create_table "Ez_Feedback", primary_key: "feedback_id", force: :cascade do |t|
    t.integer       "company_id",     limit: 4
    t.varchar       "fullname",       limit: 100,        null: false
    t.varchar       "fname",          limit: 50
    t.varchar       "lname",          limit: 50
    t.varchar       "email",          limit: 150,        null: false
    t.varchar       "phone",          limit: 30
    t.varchar       "subject",        limit: 200
    t.varchar       "type",           limit: 200
    t.varchar       "custom_field",   limit: 200
    t.text_basic    "comments",       limit: 2147483647
    t.smalldatetime "ez_createstamp",                    null: false
    t.smalldatetime "ez_updatestamp",                    null: false
    t.ss_timestamp  "ez_timestamp",                      null: false
  end

  create_table "Ez_Ff", primary_key: "ez_ff_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.integer       "ez_travel_type_id",             limit: 4,                   null: false
    t.varchar       "ez_membership_with",            limit: 30
    t.varchar       "ez_membership_code",            limit: 5
    t.varchar       "ez_membership_num",             limit: 60
    t.varchar       "ez_membership_program",         limit: 50
    t.varchar       "ez_order",                      limit: 10
    t.varchar       "ez_account_number",             limit: 15
    t.integer       "ez_miles_points",               limit: 4
    t.smalldatetime "ez_ff_miles_exp"
    t.varchar       "ez_pin_number",                 limit: 5
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Ff_Action_Bridge", primary_key: "ez_ff_action_bridge_id", force: :cascade do |t|
    t.integer       "ez_ff_donation_id", limit: 4,                 null: false
    t.integer       "ez_accessories_id", limit: 4,                 null: false
    t.smalldatetime "ez_createstamp",                              null: false
    t.smalldatetime "ez_updatestamp",                              null: false
    t.ss_timestamp  "ez_timestamp",                                null: false
    t.boolean       "ez_active",                   default: false, null: false
    t.boolean       "ez_hidden",                   default: true,  null: false
  end

  create_table "Ez_Ff_Donation", primary_key: "ez_ff_donation_id", force: :cascade do |t|
    t.integer       "ez_user_id",        limit: 4,                  null: false
    t.varchar       "ez_air_preference", limit: 30
    t.varchar       "ez_frequent_flyer", limit: 30
    t.varchar       "ez_ff_program",     limit: 30
    t.smalldatetime "ez_ff_miles_exp"
    t.varchar       "ez_account_num",    limit: 15
    t.integer       "ez_miles_points",   limit: 4
    t.varchar       "ez_pin_number",     limit: 5
    t.smalldatetime "ez_createstamp",                               null: false
    t.smalldatetime "ez_updatestamp",                               null: false
    t.ss_timestamp  "ez_timestamp",                                 null: false
    t.boolean       "ez_active",                    default: true,  null: false
    t.boolean       "ez_hidden",                    default: false, null: false
  end

  create_table "Ez_Fieldmap", primary_key: "ez_fieldmap_id", force: :cascade do |t|
    t.integer       "ez_company_id",          limit: 4
    t.varchar       "ez_record_type",         limit: 10
    t.char          "ez_map_type",            limit: 25
    t.string        "ez_field_label_name",    limit: 300,                        null: false
    t.varchar       "ez_field_table_name",    limit: 150
    t.varchar       "ez_field_name",          limit: 150
    t.varchar       "ez_field_type",          limit: 150
    t.varchar_max   "ez_field_default_value", limit: 2147483647
    t.integer       "ez_field_pos",           limit: 2
    t.varchar       "ez_gdsfield_label_name", limit: 150
    t.varchar       "ez_olbfield_label_name", limit: 150
    t.varchar       "ez_html_label_name",     limit: 150
    t.varchar_max   "stringReplacement",      limit: 2147483647
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timesstamp",                                             null: false
    t.boolean       "ez_active",                                 default: true,  null: false
    t.boolean       "ez_hidden",                                 default: false, null: false
  end

  create_table "Ez_File", primary_key: "ez_file_id", force: :cascade do |t|
    t.integer  "ts_employee_id", limit: 4
    t.integer  "ez_project_id",  limit: 4
    t.varchar  "filename",       limit: 50
    t.varchar  "displayname",    limit: 100
    t.datetime "createdat"
    t.datetime "updatedat"
    t.datetime "deletedat"
  end

  create_table "Ez_File_Mngt", primary_key: "ez_file_mngt_id", force: :cascade do |t|
    t.integer       "ez_company_id",        limit: 4
    t.integer       "ez_product_id",        limit: 4
    t.integer       "ez_event_id",          limit: 4
    t.integer       "ez_issue_id",          limit: 4
    t.integer       "ez_project_id",        limit: 4
    t.integer       "ez_user_id",           limit: 4
    t.varchar       "ez_product_toolset",   limit: 10
    t.varchar       "ez_product_icon",      limit: 50
    t.string        "secondary_directory",  limit: 100,        default: "home"
    t.string        "ez_file_name",         limit: 250
    t.text          "ez_file_description",  limit: 2147483647
    t.varchar       "ez_file_path",         limit: 150
    t.string        "ez_file_alias",        limit: 250
    t.varchar       "ez_file_type",         limit: 50
    t.varchar       "ez_file_size",         limit: 150
    t.string        "ez_file_category",     limit: 150
    t.boolean       "ez_sent_announcement",                    default: false
    t.smalldatetime "ez_announcement_date"
    t.varchar       "ez_rating",            limit: 2
    t.string        "manage_by",            limit: 200
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                               default: true,   null: false
    t.boolean       "ez_hidden",                               default: false,  null: false
  end

  create_table "Ez_Fld_Def", primary_key: "ez_fld_def_id", force: :cascade do |t|
    t.varchar       "ez_fld_def_name",        limit: 250,                        null: false
    t.varchar       "ez_fld_def_description", limit: 250
    t.text_basic    "ez_fld_def_code",        limit: 2147483647,                 null: false
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Ftp_Action_Types", primary_key: "ez_ftp_action_type_id", force: :cascade do |t|
    t.varchar "ez_ftp_action_type", limit: 50
  end

  create_table "Ez_Ftp_Actions", primary_key: "ez_ftp_action_id", force: :cascade do |t|
    t.integer  "ez_ftp_action_type_id", limit: 4
    t.integer  "ez_ftp_user_id",        limit: 4
    t.varchar  "filename",              limit: 100
    t.datetime "action_date"
  end

  create_table "Ez_Ftp_Users", primary_key: "ez_ftp_user_id", force: :cascade do |t|
    t.varchar       "ez_ftp_user",    limit: 50
    t.integer       "ez_country_id",  limit: 4
    t.integer       "ez_company_id",  limit: 4
    t.smalldatetime "ez_createstamp"
    t.smalldatetime "ez_updatestamp"
    t.boolean       "ez_active"
  end

  create_table "Ez_Gov", primary_key: "ez_gov_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.varchar       "ez_gov_issue_number",           limit: 30
    t.varchar       "ez_gov_fname",                  limit: 100
    t.varchar       "ez_gov_mname",                  limit: 100
    t.varchar       "ez_gov_lname",                  limit: 100
    t.varchar       "issue_country",                 limit: 100
    t.varchar       "place_of_issue",                limit: 100
    t.smalldatetime "date_of_issue"
    t.smalldatetime "exp_date"
    t.varchar       "ez_gov_type",                   limit: 30
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Group", primary_key: "ez_group_id", force: :cascade do |t|
    t.integer       "ez_company_id",        limit: 4
    t.integer       "ez_event_id",          limit: 4
    t.varchar       "ez_group_name",        limit: 100
    t.varchar       "ez_group_description", limit: 200
    t.smalldatetime "ez_createstamp",                                   null: false
    t.smalldatetime "ez_updatestamp",                                   null: false
    t.ss_timestamp  "ez_timestamp",                                     null: false
    t.boolean       "ez_active",                        default: true
    t.boolean       "ez_hidden",                        default: false
  end

  create_table "Ez_Group_Action_Bridge", primary_key: "ez_group_action_bridge_id", force: :cascade do |t|
    t.integer       "ez_user_id",     limit: 4
    t.integer       "ez_group_id",    limit: 4
    t.smalldatetime "ez_createstamp",                           null: false
    t.smalldatetime "ez_updatestamp",                           null: false
    t.ss_timestamp  "ez_timestamp",                             null: false
    t.boolean       "ez_active",                default: true
    t.boolean       "ez_hidden",                default: false
  end

  create_table "Ez_Group_Subset", primary_key: "ez_group_subset_id", force: :cascade do |t|
    t.integer       "ez_group_id",        limit: 4,                 null: false
    t.integer       "belong_to_group_id", limit: 4,                 null: false
    t.smalldatetime "ez_createstamp",                               null: false
    t.smalldatetime "ez_updatestamp",                               null: false
    t.ss_timestamp  "ez_timestamp",                                 null: false
    t.boolean       "ez_active",                    default: true
    t.boolean       "ez_hidden",                    default: false
  end

  create_table "Ez_HRFeed_AeTM", primary_key: "ez_aetm_id", force: :cascade do |t|
    t.integer  "ez_file_id",         limit: 4,                    null: false
    t.integer  "ez_farm_id",         limit: 4,                    null: false
    t.varchar  "sub_site_indicator", limit: 75, default: "ADMIN"
    t.varchar  "site_code",          limit: 8,                    null: false
    t.boolean  "is_active",                     default: true,    null: false
    t.datetime "createstamp",                                     null: false
    t.datetime "updatestamp",                                     null: false
  end

# Could not dump table "Ez_HRFeed_AeTM_Custom_Field_Mapping" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_AeTM_C__2C2A3D86'''

# Could not dump table "Ez_HRFeed_AeTM_Farm" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_AeTM_F__0CB1922D'''

# Could not dump table "Ez_HRFeed_Files" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_Files__2E079900'''

# Could not dump table "Ez_HRFeed_Lk_Custom_Authorizer" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_Lk_Cus__1BF3D5BD'''

# Could not dump table "Ez_HRFeed_Lk_Custom_Domain" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_Lk_Cus__33CB5F4E'''

# Could not dump table "Ez_HRFeed_Lk_Welcome_Email" because of following ActiveRecord::StatementInvalid
#   ODBC::Error: S0022 (207) [unixODBC][FreeTDS][SQL Server]Invalid column name 'filter_definition'.: EXEC sp_executesql N'SELECT [filter_definition] FROM sys.indexes WHERE name = N''UQ__Ez_HRFeed_Lk_Wel__2394F785'''

  create_table "Ez_Hierarchy", primary_key: "ez_hierarchy_id", force: :cascade do |t|
    t.integer       "ez_company_id",  limit: 4,   null: false
    t.varchar       "level1",         limit: 100
    t.varchar       "level2",         limit: 100
    t.varchar       "level3",         limit: 100
    t.smalldatetime "ez_createstamp",             null: false
    t.smalldatetime "ez_updatestamp",             null: false
    t.ss_timestamp  "ez_timestamp",               null: false
  end

  create_table "Ez_History_Of_Changes", primary_key: "ez_history_of_changes_id", force: :cascade do |t|
    t.integer       "ez_event_id",            limit: 4,                   null: false
    t.integer       "ez_user_profile_id",     limit: 4,                   null: false
    t.integer       "ez_traveler_profile_id", limit: 4,                   null: false
    t.integer       "ez_dependency_id",       limit: 4,                   null: false
    t.varchar       "ez_table_name",          limit: 100
    t.varchar       "ez_field_name",          limit: 100,                 null: false
    t.varchar       "ez_field_value",         limit: 100,                 null: false
    t.integer       "ez_table_primarykey",    limit: 4,                   null: false
    t.smalldatetime "ez_createstamp",                                     null: false
    t.smalldatetime "ez_updatestamp",                                     null: false
    t.ss_timestamp  "ez_timestamp",                                       null: false
    t.boolean       "ez_active",                          default: false, null: false
  end

  create_table "Ez_Hotel", primary_key: "ez_hotel_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                             null: false
    t.integer       "ez_event_id",                   limit: 4,                             null: false
    t.integer       "ez_company_id",                 limit: 4,                             null: false
    t.varchar       "request_type",                  limit: 50
    t.boolean       "profile_required",                                 default: false
    t.varchar       "htl_required",                  limit: 10
    t.varchar       "ez_bk_agent_code",              limit: 3
    t.varchar       "ez_tk_agent_code",              limit: 3
    t.integer       "ez_dependency_id",              limit: 4
    t.varchar       "ez_dk",                         limit: 50
    t.char          "htl_property_num",              limit: 10
    t.integer       "htl_segment_num",               limit: 4,                             null: false
    t.char          "htl_segment_code",              limit: 3
    t.char          "htl_domestic",                  limit: 1,          default: "D",      null: false
    t.varchar       "htl_trip",                      limit: 15,                            null: false
    t.varchar       "htl_trip_name",                 limit: 50
    t.char          "htl_trip_type",                 limit: 1,          default: "D"
    t.varchar       "htl_vendor_name",               limit: 100
    t.varchar       "htl_vendor_name2",              limit: 100
    t.varchar       "htl_vendor_name3",              limit: 100
    t.char          "htl_vendor_code",               limit: 3
    t.varchar       "htl_arrival_city",              limit: 50,                            null: false
    t.varchar       "htl_arrival_state",             limit: 50,                            null: false
    t.smalldatetime "htl_check_in_date",                                                   null: false
    t.smalldatetime "htl_check_out_date",                                                  null: false
    t.smalldatetime "htl_issue_date"
    t.text_basic    "htl_special_request",           limit: 2147483647
    t.text_basic    "htl_dietary_request",           limit: 2147483647
    t.text_basic    "htl_justification",             limit: 2147483647
    t.varchar       "htl_room_sharing",              limit: 150
    t.varchar       "htl_smoking",                   limit: 10,         default: "N"
    t.varchar       "htl_meal",                      limit: 50
    t.varchar       "htl_bed_type",                  limit: 50
    t.float         "htl_rate"
    t.varchar       "htl_rate_preference",           limit: 20
    t.float         "service_fee"
    t.varchar       "custom_field1",                 limit: 100
    t.varchar       "custom_field2",                 limit: 100
    t.varchar       "custom_field3",                 limit: 100
    t.varchar       "custom_field4",                 limit: 100
    t.varchar       "custom_field5",                 limit: 100
    t.varchar       "custom_field6",                 limit: 100
    t.varchar       "custom_field7",                 limit: 100
    t.varchar       "custom_field8",                 limit: 100
    t.varchar       "custom_field9",                 limit: 100
    t.varchar       "custom_field10",                limit: 100
    t.char          "htl_days",                      limit: 10
    t.varchar       "htl_number_of_adults",          limit: 5
    t.varchar       "htl_number_of_children",        limit: 5
    t.varchar       "htl_number_of_rooms",           limit: 5
    t.float         "htl_ttl_cost"
    t.varchar       "htl_direct_bill",               limit: 50
    t.varchar       "htl_billing_code",              limit: 50
    t.nchar         "htl_account_code",              limit: 10
    t.char          "htl_budgeted",                  limit: 3
    t.float         "htl_budget_amt"
    t.varchar       "company_location",              limit: 100
    t.varchar       "record_locator",                limit: 40,                            null: false
    t.varchar       "gds_record_locator",            limit: 40
    t.varchar       "purpose_of_trip",               limit: 100
    t.boolean       "ez_error",                                         default: false
    t.varchar       "ez_event_authentication_value", limit: 128,                           null: false
    t.smalldatetime "ez_createstamp",                                                      null: false
    t.smalldatetime "ez_updatestamp",                                                      null: false
    t.ss_timestamp  "ez_timestamp",                                                        null: false
    t.boolean       "ez_active",                                        default: true
    t.boolean       "ez_hidden",                                        default: false
    t.boolean       "ez_save_trip",                                     default: false
    t.varchar       "ez_rec_type",                   limit: 15,         default: "ONLINE"
    t.varchar       "htlrequired",                   limit: 10
  end

  create_table "Ez_Hotel_Category", primary_key: "ez_hotel_category_id", force: :cascade do |t|
    t.integer       "ez_webpage_category_id",        limit: 4,                   null: false
    t.varchar       "ez_table_field_name",           limit: 150
    t.varchar       "ez_table_field_value",          limit: 150
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Htl_Prefered", primary_key: "ez_htl_preferred_id", force: :cascade do |t|
    t.integer       "ez_user_id",                 limit: 4
    t.varchar       "ez_vendor_number",           limit: 50
    t.varchar       "ez_vendor_name",             limit: 50
    t.varchar       "ez_vendor_code",             limit: 50
    t.varchar       "ez_vendor_addr",             limit: 150
    t.varchar       "ez_vendor_city",             limit: 100
    t.varchar       "ez_vendor_state",            limit: 50
    t.varchar       "ez_vendor_zip",              limit: 5
    t.varchar       "ez_vendor_phone",            limit: 20
    t.varchar       "ez_preferred_route_by_code", limit: 100
    t.smalldatetime "ez_createstamp",                                         null: false
    t.smalldatetime "ez_updatestamp",                                         null: false
    t.ss_timestamp  "ez_timestamp",                                           null: false
    t.boolean       "ez_active",                              default: true,  null: false
    t.boolean       "ez_hidden",                              default: false, null: false
  end

  create_table "Ez_Huntgroup", primary_key: "ez_huntgroup_id", force: :cascade do |t|
    t.char          "ez_huntgroup_color", limit: 6,                 null: false
    t.varchar       "ez_huntgroup_desc",  limit: 50,                null: false
    t.varchar       "ez_huntgroup_phone", limit: 50,                null: false
    t.smalldatetime "ez_createstamp",                               null: false
    t.smalldatetime "ez_updatestamp",                               null: false
    t.ss_timestamp  "ez_timestamp",                                 null: false
    t.boolean       "ez_active",                     default: true, null: false
  end

  create_table "Ez_Huntgroup_Agent", primary_key: "ez_huntgroup_agent_id", force: :cascade do |t|
    t.integer       "ez_huntgroup_agent_login", limit: 4,                 null: false
    t.varchar       "ez_huntgroup_agent_name",  limit: 50,                null: false
    t.smalldatetime "ez_createstamp",                                     null: false
    t.smalldatetime "ez_updatestamp",                                     null: false
    t.ss_timestamp  "ez_timestamp",                                       null: false
    t.boolean       "ez_active",                           default: true, null: false
  end

  create_table "Ez_Huntgroup_Assignment", primary_key: "ez_huntgroup_assignment_id", force: :cascade do |t|
    t.integer       "ez_huntgroup_agent_id", limit: 4,                 null: false
    t.nchar         "ez_huntgroup_id",       limit: 10,                null: false
    t.smalldatetime "ez_createstamp",                                  null: false
    t.smalldatetime "ez_updatestamp",                                  null: false
    t.ss_timestamp  "ez_timestamp",                                    null: false
    t.boolean       "ez_active",                        default: true, null: false
  end

  create_table "Ez_Huntgroup_Collection", primary_key: "ez_huntgroup_collection_id", force: :cascade do |t|
    t.varchar       "ez_huntgroup_collection_desc",      limit: 50,                null: false
    t.char          "ez_huntgroup_collection_color_hex", limit: 6,                 null: false
    t.smalldatetime "ez_createstamp",                                              null: false
    t.smalldatetime "ez_updatestamp",                                              null: false
    t.ss_timestamp  "ez_timestamp",                                                null: false
    t.boolean       "ez_active",                                    default: true, null: false
  end

  create_table "Ez_Huntgroup_Extension", primary_key: "ez_huntgroup_extension_id", force: :cascade do |t|
    t.integer       "ez_huntgroup_id",        limit: 4,                null: false
    t.integer       "ez_huntgroup_extension", limit: 4,                null: false
    t.smalldatetime "ez_createstamp",                                  null: false
    t.smalldatetime "ez_updatestamp",                                  null: false
    t.ss_timestamp  "ez_timestamp",                                    null: false
    t.boolean       "ez_active",                        default: true, null: false
  end

  create_table "Ez_Immigration", primary_key: "ez_immigration_id", force: :cascade do |t|
    t.integer       "ez_user_id",                    limit: 4,                   null: false
    t.varchar       "ez_passport_num",               limit: 30
    t.varchar       "ez_passport_fname",             limit: 100
    t.varchar       "ez_passport_mname",             limit: 100
    t.varchar       "ez_passport_lname",             limit: 100
    t.varchar       "issue_country",                 limit: 100
    t.varchar       "place_of_issue",                limit: 100
    t.smalldatetime "date_of_issue"
    t.smalldatetime "exp_date"
    t.boolean       "usa_citizenship"
    t.varchar       "ez_non_citizenship",            limit: 50
    t.char          "ez_country_code",               limit: 5
    t.varchar       "ez_visa_type",                  limit: 30
    t.varchar       "ez_govtid_type",                limit: 30
    t.string        "place_of_birth",                limit: 50
    t.boolean       "is_passport",                               default: false, null: false
    t.boolean       "is_pr_govtid",                              default: false, null: false
    t.varchar       "ez_order",                      limit: 10
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                            null: false
    t.smalldatetime "ez_updatestamp",                                            null: false
    t.ss_timestamp  "ez_timestamp",                                              null: false
    t.boolean       "ez_active",                                 default: true
    t.boolean       "ez_hidden",                                 default: false
  end

  create_table "Ez_Implementation", primary_key: "ez_project_id", force: :cascade do |t|
    t.integer       "ez_implememtation_id", limit: 4,          null: false
    t.integer       "ez_company_id",        limit: 4,          null: false
    t.integer       "ez_status_id",         limit: 2,          null: false
    t.varchar       "project_owner_name",   limit: 100,        null: false
    t.varchar       "project_owner_email",  limit: 100
    t.varchar       "project_name",         limit: 50
    t.text_basic    "project_description",  limit: 2147483647
    t.integer       "dependency_id",        limit: 4
    t.smalldatetime "ez_createstamp",                          null: false
    t.smalldatetime "ez_updatestamp",                          null: false
    t.ss_timestamp  "ez_timestamp",                            null: false
    t.boolean       "ez_active",                               null: false
    t.boolean       "ez_hidden",                               null: false
  end

  create_table "Ez_Index_Field", primary_key: "ez_index_field_id", force: :cascade do |t|
    t.varchar       "ez_index_page_name",            limit: 200
    t.varchar       "ez_index_table_name",           limit: 250,                        null: false
    t.varchar       "ez_index_field_name",           limit: 250,                        null: false
    t.varchar       "ez_index_field_type",           limit: 150
    t.text_basic    "ez_index_field_html_code",      limit: 2147483647
    t.integer       "ez_index_field_order",          limit: 4
    t.varchar       "ez_event_authentication_value", limit: 128
    t.smalldatetime "ez_createstamp",                                                   null: false
    t.smalldatetime "ez_updatestamp",                                                   null: false
    t.ss_timestamp  "ez_timestamp",                                                     null: false
    t.boolean       "ez_active",                                        default: true
    t.boolean       "ez_hidden",                                        default: false
  end

  create_table "Ez_Invitation", primary_key: "ez_invitation_id", force: :cascade do |t|
    t.integer       "ez_schedule_id",            limit: 4
    t.integer       "ez_company_id",             limit: 4,                    null: false
    t.varchar       "ez_invitation_title",       limit: 100,                  null: false
    t.varchar       "ez_invitation_description", limit: 250
    t.varchar       "ez_invitation_message",     limit: 2000,                 null: false
    t.varchar       "ez_invitation_graphic",     limit: 100
    t.smalldatetime "ez_invitation_send_date"
    t.string        "ez_reminder_title",         limit: 50
    t.varchar       "ez_reminder_description",   limit: 250
    t.varchar       "ez_reminder_message",       limit: 2000
    t.varchar       "ez_reminder_graphic",       limit: 100
    t.smalldatetime "ez_reminder_send_date"
    t.smalldatetime "ez_createstamp",                                         null: false
    t.smalldatetime "ez_updatestamp",                                         null: false
    t.ss_timestamp  "ez_timestamp",                                           null: false
    t.boolean       "ez_active",                              default: true,  null: false
    t.boolean       "ez_hidden",                              default: false, null: false
  end

  create_table "Ez_Invitee", primary_key: "ez_invitee_id", force: :cascade do |t|
    t.integer       "ez_invitation_id",         limit: 4,                   null: false
    t.varchar       "ez_invitee_fname",         limit: 150
    t.varchar       "ez_invitee_lname",         limit: 150
    t.varchar       "ez_invitee_email_address", limit: 150
    t.smalldatetime "ez_createtimestamp",                                   null: false
    t.smalldatetime "ez_updatestamp",                                       null: false
    t.ss_timestamp  "ez_timestamp",                                         null: false
    t.boolean       "ez_active",                            default: true,  null: false
    t.boolean       "ez_hidden",                            default: false, null: false
  end

  create_table "Ez_Issue", primary_key: "ez_issue_id", force: :cascade do |t|
    t.integer       "ez_company_id",          limit: 4
    t.integer       "ez_department_id",       limit: 4
    t.integer       "owner_user_id",          limit: 4
    t.integer       "submitted_user_id",      limit: 4
    t.integer       "ez_project_id",          limit: 4
    t.integer       "ez_issue_type_id",       limit: 4
    t.varchar       "ez_issue_name",          limit: 100
    t.varchar_max   "ez_issue_description",   limit: 2147483647
    t.varchar       "priority",               limit: 5
    t.varchar       "issuer_lname",           limit: 100
    t.varchar       "issuer_fname",           limit: 100
    t.boolean       "is_employee",                                                       default: false
    t.varchar       "client_phone",           limit: 50
    t.varchar       "client_email",           limit: 100
    t.text_basic    "client_issue",           limit: 2147483647
    t.varchar       "issue_forwarded_to",     limit: 25
    t.integer       "ez_status_id",           limit: 4
    t.varchar       "escalated_to",           limit: 150
    t.varchar       "previous_escalation_to", limit: 150
    t.varchar       "eta",                    limit: 50
    t.text_basic    "resolution",             limit: 2147483647
    t.smalldatetime "completed_date"
    t.integer       "total_resources",        limit: 2
    t.decimal       "resource_duration",                         precision: 3, scale: 2
    t.varchar       "create_by_user_name",    limit: 150
    t.integer       "first_contact_user_id",  limit: 4
    t.smalldatetime "ez_createstamp"
    t.smalldatetime "ez_updatestamp"
    t.boolean       "for_knowledge_base"
    t.boolean       "ez_active",                                                         default: true
    t.boolean       "ez_hidden",                                                         default: false
  end

  create_table "Ez_Issue_Type", primary_key: "ez_issue_type_id", force: :cascade do |t|
    t.integer "ez_company_id",          limit: 4
    t.integer "ez_department_id",       limit: 4,                    null: false
    t.varchar "issue_type",             limit: 50,                   null: false
    t.varchar "issue_type_description", limit: 250
    t.varchar "tiptext",                limit: 1000
    t.boolean "ez_active",                           default: false, null: false
    t.boolean "ez_hidden",                           default: false, null: false
  end

  create_table "Ez_Key", primary_key: "ez_user_id", force: :cascade do |t|
    t.integer       "ez_dependency_id",      limit: 4
    t.integer       "ez_company_id",         limit: 4,                          null: false
    t.integer       "ez_country_id",         limit: 2,                          null: false
    t.boolean       "ez_tmp_user",                              default: true,  null: false
    t.varchar       "ez_avatar_path",        limit: 200
    t.varchar       "ez_fname",              limit: 50,                         null: false
    t.varchar       "ez_mname",              limit: 50
    t.varchar       "ez_lname",              limit: 50,                         null: false
    t.varchar       "ez_email",              limit: 150,                        null: false
    t.varchar       "ez_username",           limit: 200,                        null: false
    t.varchar       "ez_shared_key",         limit: 200
    t.char          "ez_pwd",                limit: 32,                         null: false
    t.char          "ez_pre_pwd",            limit: 32
    t.integer       "ez_num_tried",          limit: 1,          default: 0,     null: false
    t.integer       "ez_num_login",          limit: 4,          default: 0
    t.smalldatetime "ez_first_login_stamp"
    t.smalldatetime "ez_last_login_stamp",                                      null: false
    t.boolean       "ez_alert_notification",                    default: false
    t.varchar       "ez_alert_reason",       limit: 100
    t.smalldatetime "ez_inactive_date"
    t.smalldatetime "ez_reset_date"
    t.boolean       "sent_to_olb",                              default: false
    t.boolean       "sent_to_gds",                              default: false
    t.char          "ez_profile_type",       limit: 10
    t.char          "ez_user_status",        limit: 10
    t.char          "ez_ftp_status",         limit: 10
    t.smalldatetime "ez_ftp_stamp"
    t.varchar       "ez_process_code",       limit: 5
    t.char          "ez_process_trigger",    limit: 1,          default: "n"
    t.text_basic    "ez_user_agent",         limit: 2147483647
    t.varchar       "ez_remote_host",        limit: 50
    t.varchar       "ez_remote_address",     limit: 50
    t.varchar       "ez_refered",            limit: 300
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                                default: true
    t.boolean       "ez_hidden",                                default: false
    t.smalldatetime "ez_hidden_date"
    t.boolean       "ez_reset",                                 default: false
    t.boolean       "is_login",                                 default: false
    t.boolean       "email_blast",                              default: false
    t.varchar       "sabre_id",              limit: 50
    t.integer       "ez_atg_agency_id",      limit: 4
    t.varchar       "cytric_id",             limit: 50
    t.varchar       "ws_last_update_date",   limit: 50
    t.ntext         "comments",              limit: 2147483647
  end

  create_table "Ez_Key_SAVE_201407291745", id: false, force: :cascade do |t|
    t.integer       "ez_user_id",            limit: 4,                          null: false
    t.integer       "ez_dependency_id",      limit: 4
    t.integer       "ez_company_id",         limit: 4,                          null: false
    t.integer       "ez_country_id",         limit: 2,                          null: false
    t.boolean       "ez_tmp_user",                              default: true,  null: false
    t.varchar       "ez_avatar_path",        limit: 200
    t.varchar       "ez_fname",              limit: 50,                         null: false
    t.varchar       "ez_mname",              limit: 50
    t.varchar       "ez_lname",              limit: 50,                         null: false
    t.varchar       "ez_email",              limit: 150,                        null: false
    t.varchar       "ez_username",           limit: 200,                        null: false
    t.varchar       "ez_shared_key",         limit: 200
    t.char          "ez_pwd",                limit: 32,                         null: false
    t.char          "ez_pre_pwd",            limit: 32
    t.integer       "ez_num_tried",          limit: 1,          default: 0,     null: false
    t.integer       "ez_num_login",          limit: 4,          default: 0
    t.smalldatetime "ez_first_login_stamp"
    t.smalldatetime "ez_last_login_stamp",                                      null: false
    t.boolean       "ez_alert_notification",                    default: false
    t.varchar       "ez_alert_reason",       limit: 100
    t.smalldatetime "ez_inactive_date"
    t.smalldatetime "ez_reset_date"
    t.boolean       "sent_to_olb",                              default: false
    t.boolean       "sent_to_gds",                              default: false
    t.char          "ez_profile_type",       limit: 10
    t.char          "ez_user_status",        limit: 10
    t.char          "ez_ftp_status",         limit: 10
    t.smalldatetime "ez_ftp_stamp"
    t.varchar       "ez_process_code",       limit: 5
    t.char          "ez_process_trigger",    limit: 1,          default: "n"
    t.text_basic    "ez_user_agent",         limit: 2147483647
    t.varchar       "ez_remote_host",        limit: 50
    t.varchar       "ez_remote_address",     limit: 50
    t.varchar       "ez_refered",            limit: 300
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                                default: true
    t.boolean       "ez_hidden",                                default: false
    t.smalldatetime "ez_hidden_date"
    t.boolean       "ez_reset",                                 default: false
    t.boolean       "is_login",                                 default: false
    t.boolean       "email_blast",                              default: false
    t.varchar       "sabre_id",              limit: 50
    t.integer       "ez_atg_agency_id",      limit: 4
    t.varchar       "cytric_id",             limit: 50
    t.varchar       "ws_last_update_date",   limit: 50
    t.ntext         "comments",              limit: 2147483647
  end

  create_table "Ez_Key_SAVE_201410290910", id: false, force: :cascade do |t|
    t.integer       "ez_user_id",            limit: 4,                          null: false
    t.integer       "ez_dependency_id",      limit: 4
    t.integer       "ez_company_id",         limit: 4,                          null: false
    t.integer       "ez_country_id",         limit: 2,                          null: false
    t.boolean       "ez_tmp_user",                              default: true,  null: false
    t.varchar       "ez_avatar_path",        limit: 200
    t.varchar       "ez_fname",              limit: 50,                         null: false
    t.varchar       "ez_mname",              limit: 50
    t.varchar       "ez_lname",              limit: 50,                         null: false
    t.varchar       "ez_email",              limit: 150,                        null: false
    t.varchar       "ez_username",           limit: 200,                        null: false
    t.varchar       "ez_shared_key",         limit: 200
    t.char          "ez_pwd",                limit: 32,                         null: false
    t.char          "ez_pre_pwd",            limit: 32
    t.integer       "ez_num_tried",          limit: 1,          default: 0,     null: false
    t.integer       "ez_num_login",          limit: 4,          default: 0
    t.smalldatetime "ez_first_login_stamp"
    t.smalldatetime "ez_last_login_stamp",                                      null: false
    t.boolean       "ez_alert_notification",                    default: false
    t.varchar       "ez_alert_reason",       limit: 100
    t.smalldatetime "ez_inactive_date"
    t.smalldatetime "ez_reset_date"
    t.boolean       "sent_to_olb",                              default: false
    t.boolean       "sent_to_gds",                              default: false
    t.char          "ez_profile_type",       limit: 10
    t.char          "ez_user_status",        limit: 10
    t.char          "ez_ftp_status",         limit: 10
    t.smalldatetime "ez_ftp_stamp"
    t.varchar       "ez_process_code",       limit: 5
    t.char          "ez_process_trigger",    limit: 1,          default: "n"
    t.text_basic    "ez_user_agent",         limit: 2147483647
    t.varchar       "ez_remote_host",        limit: 50
    t.varchar       "ez_remote_address",     limit: 50
    t.varchar       "ez_refered",            limit: 300
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                                default: true
    t.boolean       "ez_hidden",                                default: false
    t.smalldatetime "ez_hidden_date"
    t.boolean       "ez_reset",                                 default: false
    t.boolean       "is_login",                                 default: false
    t.boolean       "email_blast",                              default: false
    t.varchar       "sabre_id",              limit: 50
    t.integer       "ez_atg_agency_id",      limit: 4
    t.varchar       "cytric_id",             limit: 50
    t.varchar       "ws_last_update_date",   limit: 50
    t.ntext         "comments",              limit: 2147483647
  end

  create_table "Ez_Key_SAVE_201411072000", id: false, force: :cascade do |t|
    t.integer       "ez_user_id",            limit: 4,                          null: false
    t.integer       "ez_dependency_id",      limit: 4
    t.integer       "ez_company_id",         limit: 4,                          null: false
    t.integer       "ez_country_id",         limit: 2,                          null: false
    t.boolean       "ez_tmp_user",                              default: true,  null: false
    t.varchar       "ez_avatar_path",        limit: 200
    t.varchar       "ez_fname",              limit: 50,                         null: false
    t.varchar       "ez_mname",              limit: 50
    t.varchar       "ez_lname",              limit: 50,                         null: false
    t.varchar       "ez_email",              limit: 150,                        null: false
    t.varchar       "ez_username",           limit: 200,                        null: false
    t.varchar       "ez_shared_key",         limit: 200
    t.char          "ez_pwd",                limit: 32,                         null: false
    t.char          "ez_pre_pwd",            limit: 32
    t.integer       "ez_num_tried",          limit: 1,          default: 0,     null: false
    t.integer       "ez_num_login",          limit: 4,          default: 0
    t.smalldatetime "ez_first_login_stamp"
    t.smalldatetime "ez_last_login_stamp",                                      null: false
    t.boolean       "ez_alert_notification",                    default: false
    t.varchar       "ez_alert_reason",       limit: 100
    t.smalldatetime "ez_inactive_date"
    t.smalldatetime "ez_reset_date"
    t.boolean       "sent_to_olb",                              default: false
    t.boolean       "sent_to_gds",                              default: false
    t.char          "ez_profile_type",       limit: 10
    t.char          "ez_user_status",        limit: 10
    t.char          "ez_ftp_status",         limit: 10
    t.smalldatetime "ez_ftp_stamp"
    t.varchar       "ez_process_code",       limit: 5
    t.char          "ez_process_trigger",    limit: 1,          default: "n"
    t.text_basic    "ez_user_agent",         limit: 2147483647
    t.varchar       "ez_remote_host",        limit: 50
    t.varchar       "ez_remote_address",     limit: 50
    t.varchar       "ez_refered",            limit: 300
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                                default: true
    t.boolean       "ez_hidden",                                default: false
    t.smalldatetime "ez_hidden_date"
    t.boolean       "ez_reset",                                 default: false
    t.boolean       "is_login",                                 default: false
    t.boolean       "email_blast",                              default: false
    t.varchar       "sabre_id",              limit: 50
    t.integer       "ez_atg_agency_id",      limit: 4
    t.varchar       "cytric_id",             limit: 50
    t.varchar       "ws_last_update_date",   limit: 50
    t.ntext         "comments",              limit: 2147483647
  end

  create_table "Ez_Key_SAVE_201412300720", id: false, force: :cascade do |t|
    t.integer       "ez_user_id",            limit: 4,                          null: false
    t.integer       "ez_dependency_id",      limit: 4
    t.integer       "ez_company_id",         limit: 4,                          null: false
    t.integer       "ez_country_id",         limit: 2,                          null: false
    t.boolean       "ez_tmp_user",                              default: true,  null: false
    t.varchar       "ez_avatar_path",        limit: 200
    t.varchar       "ez_fname",              limit: 50,                         null: false
    t.varchar       "ez_mname",              limit: 50
    t.varchar       "ez_lname",              limit: 50,                         null: false
    t.varchar       "ez_email",              limit: 150,                        null: false
    t.varchar       "ez_username",           limit: 200,                        null: false
    t.varchar       "ez_shared_key",         limit: 200
    t.char          "ez_pwd",                limit: 32,                         null: false
    t.char          "ez_pre_pwd",            limit: 32
    t.integer       "ez_num_tried",          limit: 1,          default: 0,     null: false
    t.integer       "ez_num_login",          limit: 4,          default: 0
    t.smalldatetime "ez_first_login_stamp"
    t.smalldatetime "ez_last_login_stamp",                                      null: false
    t.boolean       "ez_alert_notification",                    default: false
    t.varchar       "ez_alert_reason",       limit: 100
    t.smalldatetime "ez_inactive_date"
    t.smalldatetime "ez_reset_date"
    t.boolean       "sent_to_olb",                              default: false
    t.boolean       "sent_to_gds",                              default: false
    t.char          "ez_profile_type",       limit: 10
    t.char          "ez_user_status",        limit: 10
    t.char          "ez_ftp_status",         limit: 10
    t.smalldatetime "ez_ftp_stamp"
    t.varchar       "ez_process_code",       limit: 5
    t.char          "ez_process_trigger",    limit: 1,          default: "n"
    t.text_basic    "ez_user_agent",         limit: 2147483647
    t.varchar       "ez_remote_host",        limit: 50
    t.varchar       "ez_remote_address",     limit: 50
    t.varchar       "ez_refered",            limit: 300
    t.smalldatetime "ez_createstamp",                                           null: false
    t.smalldatetime "ez_updatestamp",                                           null: false
    t.ss_timestamp  "ez_timestamp",                                             null: false
    t.boolean       "ez_active",                                default: true
    t.boolean       "ez_hidden",                                default: false
    t.smalldatetime "ez_hidden_date"
    t.boolean       "ez_reset",                                 default: false
    t.boolean       "is_login",                                 default: false
    t.boolean       "email_blast",                              default: false
    t.varchar       "sabre_id",              limit: 50
    t.integer       "ez_atg_agency_id",      limit: 4
    t.varchar       "cytric_id",             limit: 50
    t.varchar       "ws_last_update_date",   limit: 50
    t.ntext         "comments",              limit: 2147483647
  end

