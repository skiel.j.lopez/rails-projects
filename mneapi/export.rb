# encoding UTF-8
#!/usr/bin/env ruby

require 'rubygems'
require 'bundler/setup'
Bundler.require(:db)

ActiveRecord::Base.establish_connection YAML.load_file(
  File.join(
    File.dirname(__FILE__),
    'config',
    'database.yml'
  )
)['development']

$: << File.join(File.dirname(__FILE__), 'app', 'models')
$: << File.join(File.dirname(__FILE__), 'lib')

require 'EzKey'

puts "We are in: #{EzKey.count}"
